package threads.server.core;


import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Binder;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract;
import android.provider.OpenableColumns;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;

import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.cid.Dir;
import threads.lite.cid.IPV;
import threads.lite.cid.Multiaddr;
import threads.lite.core.Link;
import threads.lite.core.Page;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.server.LogUtils;
import threads.server.R;
import threads.server.core.events.EVENTS;
import threads.server.core.files.FILES;
import threads.server.core.files.FileInfo;
import threads.server.services.MimeTypeService;

public class DOCS {
    public static final String IDX = "idx";
    public static final String URI = "uri";
    public static final String IPNS = "ipns";
    public static final String URL = "url";
    public static final String FILE = "file";

    public static final int CLICK_OFFSET = 500;
    private static final String TAG = DOCS.class.getSimpleName();
    private static volatile DOCS INSTANCE = null;
    private final ReentrantLock lock = new ReentrantLock();
    private final IPFS ipfs;
    private final FILES files;
    private final EVENTS events;
    private final String host;
    private final Session session;
    private final Server server;

    private Reachability reachability = Reachability.UNKNOWN;


    private DOCS(@NonNull Context context) throws Exception {
        ipfs = IPFS.getInstance(context);
        files = FILES.getInstance(context);
        events = EVENTS.getInstance(context);
        session = ipfs.createSession();
        try {
            host = ipfs.self().toBase36();
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }

        server = ipfs.startServer(5001, connection -> {
                },
                connection -> EVENTS.getInstance(context).connections(), peerId -> false);

    }

    public static DOCS getInstance(@NonNull Context context) throws Exception {

        if (INSTANCE == null) {
            synchronized (DOCS.class) {
                if (INSTANCE == null) {
                    INSTANCE = new DOCS(context);
                }
            }
        }
        return INSTANCE;
    }

    private static String getNameWithoutExtension(@NonNull String file) {
        String fileName = new File(file).getName();
        int dotIndex = fileName.lastIndexOf('.');
        return (dotIndex == -1) ? fileName : fileName.substring(0, dotIndex);
    }

    private static String getFileExtension(@NonNull String fullName) {
        String fileName = new File(fullName).getName();
        int dotIndex = fileName.lastIndexOf('.');
        return (dotIndex == -1) ? "" : fileName.substring(dotIndex + 1);
    }

    public static String getCompactString(@NonNull String title) {
        return title.replace("\n", " ");
    }

    @NonNull
    public static String getSize(@NonNull FileInfo fileInfo) {

        String fileSize;
        long size = fileInfo.size();

        if (size < 1000) {
            fileSize = String.valueOf(size);
            return fileSize + " B";
        } else if (size < 1000 * 1000) {
            fileSize = String.valueOf((double) (size / 1000));
            return fileSize + " KB";
        } else {
            fileSize = String.valueOf((double) (size / (1000 * 1000)));
            return fileSize + " MB";
        }
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean hasReadPermission(@NonNull Context context, @NonNull Uri uri) {
        int perm = context.checkUriPermission(uri, Binder.getCallingPid(), Binder.getCallingUid(),
                Intent.FLAG_GRANT_READ_URI_PERMISSION);
        return perm != PackageManager.PERMISSION_DENIED;
    }

    @NonNull
    public static String getMimeType(@NonNull Context context, @NonNull Uri uri) {
        String mimeType = context.getContentResolver().getType(uri);
        if (mimeType == null) {
            mimeType = MimeTypeService.OCTET_MIME_TYPE;
        }
        return mimeType;
    }

    @NonNull
    public static String getFileName(@NonNull Context context, @NonNull Uri uri) {
        String filename = null;

        ContentResolver contentResolver = context.getContentResolver();
        try (Cursor cursor = contentResolver.query(uri,
                null, null, null, null)) {

            Objects.requireNonNull(cursor);
            cursor.moveToFirst();
            int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
            filename = cursor.getString(nameIndex);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        if (filename == null) {
            filename = uri.getLastPathSegment();
        }

        if (filename == null) {
            filename = "file_name_not_detected";
        }

        return filename;
    }

    public static long getFileSize(@NonNull Context context, @NonNull Uri uri) {

        ContentResolver contentResolver = context.getContentResolver();

        try (Cursor cursor = contentResolver.query(uri,
                null, null, null, null)) {

            Objects.requireNonNull(cursor);
            cursor.moveToFirst();
            int nameIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
            return cursor.getLong(nameIndex);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }


        try (ParcelFileDescriptor fd = contentResolver.openFileDescriptor(uri, "r")) {
            Objects.requireNonNull(fd);
            return fd.getStatSize();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return -1;
    }

    public static boolean isPartial(@NonNull Context context, @NonNull Uri uri) {
        ContentResolver contentResolver = context.getContentResolver();
        try (Cursor cursor = contentResolver.query(uri, new String[]{
                DocumentsContract.Document.COLUMN_FLAGS}, null, null, null)) {

            Objects.requireNonNull(cursor);
            cursor.moveToFirst();

            int docFlags = cursor.getInt(0);
            if ((docFlags & DocumentsContract.Document.FLAG_PARTIAL) != 0) {
                return true;
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return false;
    }

    @NonNull
    public static File createTempFile(@NonNull Context context) throws IOException {
        return File.createTempFile("tmp", ".data", context.getCacheDir());
    }

    @NonNull
    public static File getTempFile(@NonNull Context context, @NonNull String filename) {
        return new File(context.getCacheDir(), filename);
    }

    @NonNull
    private static String getUniqueName(Set<String> names, @NonNull String name) {
        return getName(names, name, 0);
    }

    @NonNull
    private static String getName(Set<String> names, @NonNull String name, int index) {
        String searchName = name;
        if (index > 0) {
            try {
                String base = getNameWithoutExtension(name);
                String extension = getFileExtension(name);
                if (extension.isEmpty()) {
                    searchName = searchName.concat(" (" + index + ")");
                } else {
                    String end = " (" + index + ")";
                    if (base.endsWith(end)) {
                        String realBase = base.substring(0, base.length() - end.length());
                        searchName = realBase.concat(" (" + index + ")").concat(".").concat(extension);
                    } else {
                        searchName = base.concat(" (" + index + ")").concat(".").concat(extension);
                    }
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
                searchName = searchName.concat(" (" + index + ")"); // just backup
            }
        }

        if (names.contains(searchName)) {
            return getName(names, name, ++index);
        }
        return searchName;
    }

    private static String checkMimeType(@Nullable String mimeType, @NonNull String name) {
        boolean evalDisplayName = false;
        if (mimeType == null) {
            evalDisplayName = true;
        } else {
            if (mimeType.isEmpty()) {
                evalDisplayName = true;
            } else {
                if (Objects.equals(mimeType, MimeTypeService.OCTET_MIME_TYPE)) {
                    evalDisplayName = true;
                }
            }
        }
        if (evalDisplayName) {
            mimeType = MimeTypeService.getMimeType(name);
        }
        return mimeType;
    }

    @NonNull
    private static Set<String> linkNames(@NonNull Session session, @NonNull Cid cid) throws Exception {
        Set<String> names = new HashSet<>();
        List<Link> links = IPFS.links(session, cid, false, () -> false);
        for (Link link : links) {
            names.add(link.name());
        }
        return names;
    }

    @NonNull
    public Set<Multiaddr> dialableAddresses() {
        return server.dialableAddresses();
    }

    public long createDirectory(long parent, @NonNull String name) throws Exception {
        lock.lock();
        try {
            Dir directory = IPFS.createEmptyDirectory(session);
            long idx = createDocument(parent, MimeTypeService.DIR_MIME_TYPE,
                    directory.cid(), null, name, directory.size());
            finishDocument(idx);
            return idx;
        } finally {
            lock.unlock();
        }
    }

    public void createTextFile(long parent, @NonNull String text) {
        lock.lock();
        try {
            Cid cid = IPFS.storeText(session, text);
            String timeStamp = DateFormat.getDateTimeInstance().
                    format(new Date()).
                    replace(":", "").
                    replace(".", "_").
                    replace("/", "_").
                    replace(" ", "_");

            String name = "TXT_" + timeStamp + ".txt";

            long idx = createDocument(parent, MimeTypeService.PLAIN_MIME_TYPE, cid,
                    null, name, text.length());

            finishDocument(idx);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            lock.unlock();
        }
    }

    @NonNull
    public Uri getHomePageUri() {
        return Uri.parse(DOCS.IPNS + "://" + host);
    }

    public void deleteDocuments(long... idxs) {
        lock.lock();
        try {
            removeFromParentDocument(idxs);
            deleteRecursiveDocuments(idxs);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            lock.unlock();
        }
    }

    private void removeFromParentDocument(long... idxs) {
        for (long idx : idxs) {
            try {
                removeFromParentDocument(idx);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }
    }

    private void deleteRecursiveDocuments(long... idxs) {
        try {
            for (long idx : idxs) {
                List<FileInfo> children = files.getChildren(idx);
                for (FileInfo fileInfo : children) {
                    deleteRecursiveDocuments(fileInfo.idx());
                }
            }
            files.finalDeleting(idxs);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    private Set<String> linkNames(@NonNull Session session, long parent) throws Exception {
        Cid dirCid;
        if (parent > 0) {
            // child is located in a directory
            dirCid = Objects.requireNonNull(files.getContent(parent));
        } else {
            // child is top level file (normally nothing to do, but we will update the page content)
            dirCid = ipfs.getPageStore().getPageContent(ipfs.self());
        }
        Objects.requireNonNull(dirCid);
        return linkNames(session, dirCid);

    }

    public void finishDocument(long idx) throws Exception {
        lock.lock();
        try {
            updateParent(idx);
        } finally {
            lock.unlock();
        }
    }

    private void updateParent(long idx) throws Exception {

        FileInfo child = files.getFileInfo(idx);
        Objects.requireNonNull(child);

        long parent = child.parent();

        Cid cid = child.cid();
        Objects.requireNonNull(cid);

        if (parent > 0) {
            // child is located in a directory
            Cid dirCid = Objects.requireNonNull(files.getContent(parent));
            Objects.requireNonNull(dirCid);

            Dir directory = IPFS.updateLinkToDirectory(session, new Dir(dirCid, 0L), // 0L is not nice [but does not matter]
                    Link.create(cid, child.name(), child.size(), Link.Unknown)); // Link.Unknown is not nice [but does not matter]
            Objects.requireNonNull(directory);

            files.updateContent(parent, directory.cid(), directory.size(),
                    System.currentTimeMillis());
            updateParent(parent);
        } else {
            // child is top level file (normally nothing to do, but we will update the page content)
            Cid dirCid = ipfs.getPageStore().getPageContent(ipfs.self());
            Objects.requireNonNull(dirCid);

            Dir directory = IPFS.updateLinkToDirectory(session, new Dir(dirCid, 0L), // 0L is not nice [but does not matter]
                    Link.create(cid, child.name(), child.size(), Link.Unknown)); // Link.Unknown is not nice [but does not matter]
            Objects.requireNonNull(directory);

            ipfs.getPageStore().updatePageContent(ipfs.self(), directory.cid(),
                    Page.getDefaultEol());
        }
    }

    private void removeFromParentDocument(long idx) throws Exception {
        FileInfo child = files.getFileInfo(idx);
        if (child != null) {
            String name = child.name();
            long parent = child.parent();
            if (parent > 0) {
                Cid dirCid = Objects.requireNonNull(files.getContent(parent));
                Objects.requireNonNull(dirCid);
                Dir newDir = IPFS.removeFromDirectory(session, new Dir(dirCid, 0L), name);
                files.updateContent(parent, newDir.cid(),
                        newDir.size(), System.currentTimeMillis());
                updateParent(parent);
            } else {
                Cid dirCid = ipfs.getPageStore().getPageContent(ipfs.self());
                Objects.requireNonNull(dirCid);
                Dir newDir = IPFS.removeFromDirectory(session, new Dir(dirCid, 0L), name);
                ipfs.getPageStore().updatePageContent(ipfs.self(), newDir.cid(),
                        Page.getDefaultEol());
            }
        }
    }

    public long createDocument(long parent, @Nullable String type,
                               @Nullable Cid cid, @Nullable Uri uri, @NonNull String displayName,
                               long size) throws Exception {
        Set<String> names = linkNames(session, parent);
        String name = getUniqueName(names, displayName);
        String mimeType = checkMimeType(type, displayName);
        FileInfo fileInfo = FILES.createFileInfo(name, parent, mimeType, cid, size, uri);
        return files.storeFileInfo(fileInfo);
    }


    public void initPinsPage(@Nullable String name) {
        lock.lock();

        try {
            Page page = getHomePage();


            if (page == null) {
                Dir dir = IPFS.createEmptyDirectory(session);
                Objects.requireNonNull(dir);
                page = new Page(ipfs.self(), IPFS.LITE_PULL_PROTOCOL, name,
                        Page.getDefaultEol().getTime(),
                        Page.encodeIpnsData(dir.cid()),
                        System.currentTimeMillis());
                ipfs.getPageStore().storePage(page);
            }

            Objects.requireNonNull(page);

            List<FileInfo> pins = files.getPins();

            List<Link> fileLinks = new ArrayList<>();
            boolean isEmpty = pins.isEmpty();
            if (!isEmpty) {
                for (FileInfo pin : pins) {
                    Cid link = pin.cid();
                    Objects.requireNonNull(link);
                    fileLinks.add(Link.create(link, pin.name(), pin.size(), Link.Unknown));
                }
            }
            Dir dir = IPFS.createDirectory(session, fileLinks);
            Objects.requireNonNull(dir);


            page = new Page(ipfs.self(), IPFS.LITE_PULL_PROTOCOL, name,
                    Page.getDefaultEol().getTime(),
                    Page.encodeIpnsData(dir.cid()), System.currentTimeMillis());
            ipfs.getPageStore().storePage(page);


        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            lock.unlock();
        }
    }


    @NonNull
    public Uri getIpnsUri(@NonNull FileInfo fileInfo) {

        Uri.Builder builder = new Uri.Builder();
        builder.scheme(DOCS.IPNS)
                .authority(host);
        List<FileInfo> ancestors = files.getAncestors(fileInfo.idx());
        for (FileInfo ancestor : ancestors) {
            builder.appendPath(ancestor.name());
        }
        return builder.build();
    }


    @Nullable
    public Page getHomePage() {
        return ipfs.getPageStore().getPage(ipfs.self());
    }

    @NonNull
    public Server getServer() {
        return server;
    }

    public void setReachability(Reachability reachability) {
        if (reachability != this.reachability) {
            this.reachability = reachability;
            events.reachability(reachability.name());
        }
    }

    @NonNull
    public IPV ipv() {
        return ipfs.ipv().get();
    }

    public String getNetworkReachability(@NonNull Context context) {
        return switch (reachability) {
            case UNKNOWN -> context.getString(R.string.unknown);
            case LOCAL -> context.getString(R.string.local_network);
            case RELAYS -> context.getString(R.string.relays_network);
            case GLOBAL -> context.getString(R.string.global_network);
        };
    }

    public void updateNetwork() {
        server.updateNetwork();
    }


    public enum Reachability {
        UNKNOWN, LOCAL, RELAYS, GLOBAL
    }
}
