package threads.server.core.files;

import android.provider.DocumentsContract;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Objects;
import java.util.UUID;

import threads.lite.cid.Cid;

/**
 * @param parent       checked
 * @param name         checked
 * @param idx          checked
 * @param lastModified checked
 * @param cid          checked
 * @param size         checked
 * @param mimeType     checked
 * @param uri          checked
 * @param work         checked
 * @param deleting     checked
 */
@Entity
public record FileInfo(@PrimaryKey(autoGenerate = true) long idx,
                       @NonNull @ColumnInfo(name = "name") String name,
                       @ColumnInfo(name = "parent") long parent,
                       @NonNull @ColumnInfo(name = "mimeType") String mimeType,
                       @Nullable @ColumnInfo(name = "cid") Cid cid,
                       @ColumnInfo(name = "size") long size,
                       @Nullable @ColumnInfo(name = "uri") String uri,
                       @ColumnInfo(name = "lastModified") long lastModified,
                       @Nullable @ColumnInfo(name = "work") String work,
                       @ColumnInfo(name = "deleting") boolean deleting) {


    @Override
    @Nullable
    public Cid cid() {
        return cid;
    }

    @Override
    @Nullable
    public String uri() {
        return uri;
    }

    @Override
    public long lastModified() {
        return lastModified;
    }

    @Override
    public long idx() {
        return idx;
    }

    @Override
    @NonNull
    public String mimeType() {
        return mimeType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileInfo fileInfo = (FileInfo) o;
        return idx() == fileInfo.idx();
    }

    @Override
    public int hashCode() {
        return Objects.hash(idx());
    }

    @Override
    public long parent() {
        return parent;
    }

    @Override
    public long size() {
        return size;
    }

    @Override
    @NonNull
    public String name() {
        return name;
    }

    public boolean isDir() {
        return DocumentsContract.Document.MIME_TYPE_DIR.equals(mimeType());
    }

    @Override
    public boolean deleting() {
        return deleting;
    }

    @Override
    @Nullable
    public String work() {
        return work;
    }

    @Nullable
    public UUID getWorkUUID() {
        if (work != null) {
            return UUID.fromString(work);
        }
        return null;
    }


}
