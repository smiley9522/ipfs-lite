package threads.server.core.files;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import threads.lite.cid.Cid;

@Database(entities = {FileInfo.class}, version = 2, exportSchema = false)
@TypeConverters({Cid.class})
public abstract class FileInfoDatabase extends RoomDatabase {

    public abstract FileInfoDao fileInfoDao();

}
