package threads.server.core.files;

import android.content.Context;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import threads.lite.cid.Cid;

public class FILES {

    private static volatile FILES INSTANCE = null;

    private final FileInfoDatabase fileInfoDatabase;


    private FILES(FileInfoDatabase fileInfoDatabase) {
        this.fileInfoDatabase = fileInfoDatabase;
    }

    public static FILES getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (FILES.class) {
                if (INSTANCE == null) {
                    FileInfoDatabase filesDatabase = Room.databaseBuilder(context,
                                    FileInfoDatabase.class,
                                    FileInfoDatabase.class.getSimpleName()).
                            fallbackToDestructiveMigration().
                            allowMainThreadQueries().
                            build();
                    INSTANCE = new FILES(filesDatabase);
                }
            }
        }
        return INSTANCE;
    }

    @NonNull
    public static FileInfo createFileInfo(@NonNull String name, long parent, @NonNull String mimeType,
                                          @Nullable Cid cid, long size, @Nullable Uri uri) {
        if (uri != null) {
            return new FileInfo(0, name, parent, mimeType, cid, size, uri.toString(),
                    System.currentTimeMillis(), null, false);
        }
        return new FileInfo(0, name, parent, mimeType, cid, size, null,
                System.currentTimeMillis(), null, false);
    }

    public void clear() {
        fileInfoDatabase.clearAllTables();
    }

    @NonNull
    public FileInfoDatabase getFileInfoDatabase() {
        return fileInfoDatabase;
    }

    public void setDeleting(long... idxs) {
        for (long idx : idxs) {
            fileInfoDatabase.fileInfoDao().setDeleting(idx);
        }
    }

    public void resetDeleting(long... idxs) {
        for (long idx : idxs) {
            fileInfoDatabase.fileInfoDao().resetDeleting(idx);
        }
    }

    public void setDone(long idx) {
        fileInfoDatabase.fileInfoDao().setDone(idx);
    }

    public void setDone(long idx, @NonNull Cid cid) {
        fileInfoDatabase.fileInfoDao().setDone(idx, cid);
    }

    public List<FileInfo> getAncestors(long idx) {
        List<FileInfo> path = new ArrayList<>();
        if (idx > 0) {
            FileInfo fileInfo = getFileInfo(idx);
            if (fileInfo != null) {
                path.addAll(getAncestors(fileInfo.parent()));
                path.add(fileInfo);
            }
        }
        return path;
    }

    private void delete(long idx) {
        fileInfoDatabase.fileInfoDao().delete(idx);
    }

    public void finalDeleting(long... idxs) {
        for (long idx : idxs) {
            delete(idx);
        }
    }

    public long storeFileInfo(@NonNull FileInfo fileInfo) {
        return fileInfoDatabase.fileInfoDao().insertFileInfo(fileInfo);
    }

    public void updateContent(long idx, @NonNull Cid cid, long size, long lastModified) {
        fileInfoDatabase.fileInfoDao().updateContent(idx, cid, size, lastModified);
    }

    @NonNull
    public List<FileInfo> getPins() {
        return fileInfoDatabase.fileInfoDao().getPins();
    }


    @NonNull
    public List<FileInfo> getChildren(long parent) {
        return fileInfoDatabase.fileInfoDao().getChildren(parent);
    }

    @Nullable
    public FileInfo getFileInfo(long idx) {
        return fileInfoDatabase.fileInfoDao().getFileInfo(idx);
    }

    @Nullable
    public Cid getContent(long idx) {
        return fileInfoDatabase.fileInfoDao().getContent(idx);
    }

    public void setWork(long idx, @NonNull UUID id) {
        fileInfoDatabase.fileInfoDao().setWork(idx, id.toString());
    }

    public void setUri(long idx, @NonNull String uri) {
        fileInfoDatabase.fileInfoDao().setUri(idx, uri);
    }
}
