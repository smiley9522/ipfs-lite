package threads.server.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.List;

import threads.lite.IPFS;
import threads.lite.cid.Multiaddr;
import threads.lite.core.AutonatResult;
import threads.lite.core.NatType;
import threads.lite.store.SWARM;
import threads.server.LogUtils;
import threads.server.core.DOCS;

public final class AutonatWorker extends Worker {
    private static final String TAG = AutonatWorker.class.getSimpleName();

    public AutonatWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }


    private static OneTimeWorkRequest getWork() {
        Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();

        return new OneTimeWorkRequest.Builder(AutonatWorker.class)
                .addTag(TAG)
                .setConstraints(constraints)
                .build();
    }

    public static void autonat(@NonNull Context context) {
        WorkManager.getInstance(context).enqueueUniqueWork(
                TAG, ExistingWorkPolicy.KEEP, getWork());
    }


    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        LogUtils.error(TAG, "Worker Start " + getId() + " ...");

        try {
            DOCS docs = DOCS.getInstance(getApplicationContext());
            SWARM swarm = SWARM.getInstance(getApplicationContext());
            List<Multiaddr> multiaddrs = swarm.getMultiaddrs();

            AutonatResult result;

            if (!multiaddrs.isEmpty()) {
                result = docs.getServer().autonat(multiaddrs, IPFS.AUTONAT_TIMEOUT);
            } else {
                result = docs.getServer().autonat();
            }

            if (!result.success()) {
                if (result.natType() == NatType.SYMMETRIC) {
                    docs.setReachability(DOCS.Reachability.LOCAL);
                } else {
                    docs.setReachability(DOCS.Reachability.RELAYS);
                    SwarmWorker.reservations(getApplicationContext(),
                            ExistingWorkPolicy.REPLACE, 0);
                }
            } else {
                docs.setReachability(DOCS.Reachability.GLOBAL);
                SwarmWorker.reservations(getApplicationContext(),
                        ExistingWorkPolicy.REPLACE, 0);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.error(TAG, "Worker Finish " + getId() +
                    " onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();
    }
}
