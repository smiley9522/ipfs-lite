package threads.server.utils;

import androidx.recyclerview.widget.DiffUtil;

import java.util.List;
import java.util.Objects;

import threads.server.core.files.FileInfo;

@SuppressWarnings("WeakerAccess")
public class FileDiffCallback extends DiffUtil.Callback {
    private final List<FileInfo> mOldList;
    private final List<FileInfo> mNewList;

    public FileDiffCallback(List<FileInfo> messages, List<FileInfo> fileInfos) {
        this.mOldList = messages;
        this.mNewList = fileInfos;
    }

    public static boolean areItemsTheSame(FileInfo oldInfo, FileInfo newInfo) {
        return oldInfo.idx() == newInfo.idx();
    }

    public static boolean areContentsTheSame(FileInfo oldInfo, FileInfo newInfo) {
        return oldInfo.deleting() == newInfo.deleting() &&
                oldInfo.lastModified() == newInfo.lastModified() &&
                Objects.equals(oldInfo.size(), newInfo.size()) &&
                Objects.equals(oldInfo.work(), newInfo.work()) &&
                Objects.equals(oldInfo.name(), newInfo.name()) &&
                Objects.equals(oldInfo.mimeType(), newInfo.mimeType()) &&
                Objects.equals(oldInfo.uri(), newInfo.uri());
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return areItemsTheSame(mOldList.get(oldItemPosition), mNewList.get(newItemPosition));
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return areContentsTheSame(mOldList.get(oldItemPosition), mNewList.get(newItemPosition));
    }
}
