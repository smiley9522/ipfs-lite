package threads.lite.crypto;

import androidx.annotation.NonNull;

import com.google.crypto.tink.subtle.Ed25519Verify;

import crypto.pb.Crypto;


public interface Ed25519 {

    static PubKey unmarshalEd25519PublicKey(byte[] keyBytes) {
        return new Ed25519PublicKey(keyBytes);
    }

    record Ed25519PublicKey(byte[] publicKey) implements PubKey {

        @NonNull
        public byte[] raw() {
            return this.publicKey;
        }

        public void verify(byte[] data, byte[] signature) throws Exception {
            Ed25519Verify verifier = new Ed25519Verify(publicKey);
            verifier.verify(signature, data);
        }

        @NonNull
        @Override
        public Crypto.KeyType getKeyType() {
            return Crypto.KeyType.Ed25519;
        }

    }

}
