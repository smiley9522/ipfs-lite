package threads.lite.crypto;

import androidx.annotation.NonNull;

import com.google.crypto.tink.subtle.Ed25519Sign;
import com.google.crypto.tink.subtle.Ed25519Verify;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Objects;

import crypto.pb.Crypto;
import threads.lite.IPFS;
import threads.lite.cid.Hashtype;
import threads.lite.cid.Multicodec;
import threads.lite.cid.Multihash;
import threads.lite.cid.PeerId;
import threads.lite.core.DataHandler;
import threads.lite.core.Keys;

public interface Key {

    static byte[] createIpnsKey(@NonNull PeerId peerId) {
        byte[] ipns = IPFS.IPNS_PATH.getBytes();
        byte[] selfKey = peerId.encoded();
        ByteBuffer byteBuffer = ByteBuffer.allocate(ipns.length + selfKey.length);
        byteBuffer.put(ipns);
        byteBuffer.put(selfKey);
        return byteBuffer.array();
    }

    @NonNull
    static PeerId decodeIpnsKey(byte[] ipnsKey) throws Exception {
        byte[] ipns = IPFS.IPNS_PATH.getBytes();
        int index = DataHandler.indexOf(ipnsKey, ipns);
        if (index != 0) {
            throw new Exception("parsing issue");
        }
        byte[] pid = Arrays.copyOfRange(ipnsKey, ipns.length, ipnsKey.length);
        Multihash mh = Multihash.decode(pid);
        return PeerId.create(mh.encoded());
    }


    @NonNull
    static PubKey extractPublicKey(@NonNull PeerId id) throws Exception {
        CodedInputStream wrap = CodedInputStream.newInstance(id.encoded());
        int version = wrap.readRawVarint32();
        if (version != Multicodec.IDENTITY.codec()) {
            throw new Exception("not supported multicodec");
        }
        int length = wrap.readRawVarint32();
        byte[] data = wrap.readRawBytes(length);
        return Key.unmarshalPublicKey(data);
    }

    static PubKey unmarshalPublicKey(byte[] data) throws Exception {

        Crypto.PublicKey pms = Crypto.PublicKey.parseFrom(data);

        byte[] pubKeyData = pms.getData().toByteArray();

        return switch (pms.getType()) {
            case RSA -> Rsa.unmarshalRsaPublicKey(pubKeyData);
            case ECDSA -> Ecdsa.unmarshalEcdsaPublicKey(pubKeyData);
            case Secp256k1 -> throw new IllegalStateException(
                    "not supported [jdk does not support bitcoin Secp256k1]");
            case Ed25519 -> Ed25519.unmarshalEd25519PublicKey(pubKeyData);
        };
    }

    // Note: Only Ed25519 support
    static byte[] sign(byte[] privateKey, byte[] data) throws Exception {
        Objects.requireNonNull(privateKey);
        Ed25519Sign signer = new Ed25519Sign(privateKey);
        return signer.sign(data);
    }

    // Note: Only Ed25519 support
    static Crypto.PublicKey createCryptoKey(byte[] publicKey) {
        Objects.requireNonNull(publicKey);
        return Crypto.PublicKey.newBuilder().setType(crypto.pb.Crypto.KeyType.Ed25519)
                .setData(ByteString.copyFrom(publicKey))
                .build();
    }

    // Note: Only Ed25519 support
    static PeerId createPeerId(byte[] publicKey) throws Exception {
        Ed25519.Ed25519PublicKey pubKey = new Ed25519.Ed25519PublicKey(publicKey);
        Objects.requireNonNull(pubKey);
        return fromPubKey(pubKey);
    }


    @NonNull
    static PeerId fromPubKey(@NonNull PubKey pubKey) throws Exception {
        // https://github.com/libp2p/specs/blob/master/peer-ids/peer-ids.md

        // Peer IDs are derived by hashing the encoded public key with multihash. Keys that
        // serialize to more than 42 bytes must be hashed using sha256 multihash, keys that
        // serialize to at most 42 bytes must be hashed using the "identity" multihash multicodec.
        //
        // Specifically, to compute a peer ID of a key:
        //
        // Encode the public key as described in the keys section.
        // If the length of the serialized bytes is less than or equal to 42, compute the
        // "identity" multihash of the serialized bytes. In other words, no hashing is performed,
        // but the multihash format is still followed (byte plus varint plus serialized bytes).
        // The idea here is that if the serialized byte array is short enough, we can fit it in a
        // multihash verbatim without having to condense it using a hash function.
        // If the length is greater than 42, then hash it using the SHA256 multihash.

        byte[] pubKeyBytes = Crypto.PublicKey.newBuilder().setType(pubKey.getKeyType()).
                setData(ByteString.copyFrom(pubKey.raw())).build().toByteArray();

        if (pubKeyBytes.length <= 42) {
            Multihash hash = Multihash.create(Hashtype.ID, pubKeyBytes);
            return PeerId.create(hash.encoded());
        } else {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            Multihash hash = Multihash.create(Hashtype.SHA2_256, digest.digest(pubKeyBytes));
            return PeerId.create(hash.encoded());
        }
    }

    // Note: Only Ed25519 support
    static void verify(byte[] publicKey, byte[] data, byte[] signature) throws Exception {
        // get the publicKey from the other party.
        Ed25519Verify verifier = new Ed25519Verify(publicKey);
        verifier.verify(signature, data);
    }

    // Note: Only Ed25519 support
    static Keys generateKeys() throws Exception {
        Ed25519Sign.KeyPair keyPair = Ed25519Sign.KeyPair.newKeyPair();
        return new Keys(keyPair.getPublicKey(), keyPair.getPrivateKey());
    }

    static PeerId random() {
        try {
            Keys keys = generateKeys();
            return Key.createPeerId(keys.publicKey());
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
    }


    @NonNull
    byte[] raw();
}


