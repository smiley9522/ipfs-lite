package threads.lite.core;

import bitswap.pb.MessageOuterClass;
import threads.lite.quic.Connection;

public interface BitSwap extends Swap {


    void receiveMessage(Connection connection, MessageOuterClass.Message bsm) throws Exception;


}
