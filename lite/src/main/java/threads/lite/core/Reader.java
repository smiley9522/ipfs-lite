package threads.lite.core;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.util.Objects;

import merkledag.pb.Merkledag;
import threads.lite.cid.Cid;
import threads.lite.dag.DagReader;
import threads.lite.dag.DagResolver;

public record Reader(@NonNull DagReader dagReader, @NonNull Session session,
                     @NonNull Cancellable cancellable) {


    public static Reader createReader(@NonNull Cancellable cancellable,
                                      @NonNull Session session, @NonNull Cid cid)
            throws Exception {

        Merkledag.PBNode top = DagResolver.resolveNode(session, cancellable, cid);
        Objects.requireNonNull(top);
        DagReader dagReader = DagReader.create(top);

        return new Reader(dagReader, session, cancellable);
    }

    @NonNull
    public ByteString loadNextData() throws Exception {
        return dagReader.loadNextData(session, cancellable);
    }

    public void seek(int position) throws Exception {
        dagReader.seek(session, cancellable, position);
    }

    public long getSize() {
        return this.dagReader.size();
    }
}
