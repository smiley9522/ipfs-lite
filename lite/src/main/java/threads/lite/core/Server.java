package threads.lite.core;

import androidx.annotation.NonNull;

import java.net.DatagramSocket;
import java.util.Collection;
import java.util.Set;
import java.util.function.Consumer;

import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multiaddrs;

public interface Server extends Host, Swarm {

    void shutdown();

    int getPort();

    int numServerConnections();

    @NonNull
    DatagramSocket getSocket();

    void holePunching(@NonNull Multiaddrs multiaddrs);

    @NonNull
    PeerInfo identity() throws Exception;

    @NonNull
    AutonatResult autonat();

    @NonNull
    AutonatResult autonat(Collection<Multiaddr> multiaddrs, int timeout);

    boolean hasReservations();

    Set<Reservation> reservations();

    void swarm(Cancellable cancellable);

    Reservation reservation(Multiaddr multiaddr) throws Exception;

    void reservations(Cancellable cancellable);

    Multiaddrs dialableAddresses();

    void provide(@NonNull Cid cid, @NonNull Consumer<Multiaddr> consumer,
                 @NonNull Cancellable cancellable);

    @NonNull
    NatType getNatType();

    Multiaddr getObserved();

    @NonNull
    Set<Multiaddr> publicNetworkAddresses();

    void updateNetwork();

    @NonNull
    Set<String> getProtocolNames();

}
