package threads.lite.core;

import androidx.annotation.NonNull;

import java.io.InputStream;

public final class ReaderInputStream {

    private final InputStream inputStream;
    private final Progress mProgress;
    private final long size;
    private int progress = 0;
    private long totalRead = 0;
    private boolean done;


    public ReaderInputStream(@NonNull InputStream inputStream, @NonNull Progress progress, long size) {
        this.inputStream = inputStream;
        this.mProgress = progress;
        this.size = size;
    }

    public ReaderInputStream(@NonNull InputStream inputStream, long size) {
        this.inputStream = inputStream;
        this.mProgress = null;
        this.size = size;
    }

    public int read(byte[] bytes) throws Exception {

        if (mProgress != null) {
            if (mProgress.isCancelled()) {
                throw new InterruptedException("progress closed");
            }
        }

        int read = inputStream.read(bytes);
        if (read < bytes.length) {
            done = true;
        }
        if (read > 0) {
            totalRead += read;
            if (mProgress != null) {
                if (mProgress.doProgress()) {
                    if (size > 0) {
                        int percent = (int) ((totalRead * 100.0f) / size);
                        if (progress < percent) {
                            progress = percent;
                            mProgress.setProgress(percent);
                        }
                    }
                }
            }
        }
        return read;

    }

    public boolean done() {
        return done;
    }
}
