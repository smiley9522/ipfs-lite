package threads.lite.core;

import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

import crypto.pb.Crypto;
import identify.pb.IdentifyOuterClass;
import threads.lite.cid.IPV;
import threads.lite.cid.Multiaddrs;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.host.LiteCertificate;
import threads.lite.host.LiteResponder;

public interface Host {
    Crypto.PublicKey getPublicKey();

    Multiaddrs publishMultiaddrs();

    Set<Peer> getRoutingPeers();

    PeerStore getPeerStore();

    PeerId self();

    Map<String, ProtocolHandler> getProtocols();

    LiteCertificate getLiteCertificate();

    LiteResponder getLiteResponder();

    byte[] getPrivateKey();

    IdentifyOuterClass.Identify identify();

    Supplier<IPV> ipv();

}
