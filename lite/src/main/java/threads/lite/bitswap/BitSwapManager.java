package threads.lite.bitswap;


import androidx.annotation.NonNull;

import java.net.ConnectException;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

import bitswap.pb.MessageOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Block;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.BitSwap;
import threads.lite.core.BlockStore;
import threads.lite.core.Cancellable;
import threads.lite.core.DataHandler;
import threads.lite.core.Session;
import threads.lite.quic.Connection;
import threads.lite.quic.Parameters;
import threads.lite.quic.StreamRequester;


public final class BitSwapManager implements BitSwap {

    private static final String TAG = BitSwapManager.class.getSimpleName();
    @NonNull
    private final Session session;
    @NonNull
    private final BlockStore blockStore;
    @NonNull
    private final BitSwapEngine bitSwapEngine;
    @NonNull
    private final BitSwapRegistry bitSwapRegistry = new BitSwapRegistry();

    @NonNull
    private final AtomicBoolean closed = new AtomicBoolean(false);

    public BitSwapManager(@NonNull Session session) {
        this.session = session;
        this.blockStore = session.getBlockStore();
        this.bitSwapEngine = new BitSwapEngine(blockStore);
    }

    private static CompletableFuture<Void> writeMessage(@NonNull Connection connection,
                                                        @NonNull MessageOuterClass.Message message,
                                                        int timeout) {

        CompletableFuture<Void> done = new CompletableFuture<>();

        try {
            StreamRequester.createStream(connection, new BitSwapRequest(done))
                    .request(DataHandler.encode(message,
                            IPFS.MULTISTREAM_PROTOCOL, IPFS.BITSWAP_PROTOCOL), timeout);
        } catch (Throwable throwable) {
            done.completeExceptionally(throwable);
        }
        return done;

    }

    @Override
    public void close() {
        try {
            closed.set(true);
            bitSwapRegistry.close();
            bitSwapEngine.close();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    private void connectProvider(Cancellable cancellable, Multiaddr multiaddr) {


        if (session.getConnection(multiaddr.peerId()) != null) {
            // there is already a connection with that peerID
            // so just return
            return;
        }

        if (cancellable.isCancelled()) {
            return;
        }

        try {
            LogUtils.info(TAG, "Try connection " + multiaddr);
            session.dial(multiaddr, Parameters.getDefault());
            LogUtils.info(TAG, "New connection " + multiaddr);
        } catch (ConnectException | TimeoutException e) {
            LogUtils.error(TAG, "Failure ConnectException connection " + multiaddr);
        } catch (InterruptedException ignore) {
            // ignore
        }


    }


    @NonNull
    private Block runWantHaves(@NonNull Cancellable cancellable, @NonNull Cid cid) throws Exception {

        bitSwapRegistry.register(cid);

        AtomicBoolean providerStart = new AtomicBoolean(false);

        // TODO [Future urgent] replace by virtual threads
        ScheduledExecutorService timer = Executors.newSingleThreadScheduledExecutor();

        MessageOuterClass.Message haveMessage = BitSwapMessage.create(
                MessageOuterClass.Message.Wantlist.WantType.Have, cid);
        try {
            Set<PeerId> haves = new HashSet<>();


            while (!blockStore.hasBlock(cid)) {

                for (Connection connection : session.swarm()) {

                    if (haves.add(connection.remotePeerId())) {
                        // connection with remotePeerId can only enter once

                        LogUtils.info(TAG, "Schedule Haves " +
                                cid + " " + connection.remotePeerId());

                        writeMessage(connection, haveMessage, IPFS.DEFAULT_REQUEST_TIMEOUT)
                                .whenComplete((unused, throwable) -> {
                                    if (throwable != null) {
                                        LogUtils.error(TAG, throwable.getMessage());
                                    }
                                });
                    }
                }

                if (cancellable.isCancelled()) {
                    throw new InterruptedException("canceled operation");
                }


                if (!providerStart.getAndSet(true)) {

                    int delay = 0;
                    if (!haves.isEmpty()) {
                        delay = IPFS.BITSWAP_REQUEST_DELAY * 2;
                    }

                    timer.schedule(() -> {

                        LogUtils.debug(TAG, "Load Provider Start " + cid);

                        long start = System.currentTimeMillis();

                        try {
                            session.findProviders(cancellable,
                                    (multiaddr) -> connectProvider(cancellable, multiaddr), cid);
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable.getClass().getSimpleName());
                        }

                        LogUtils.debug(TAG, "Load Provider Finish "
                                + cid +
                                " onStart [" +
                                (System.currentTimeMillis() - start) +
                                "]...");


                        if (!cancellable.isCancelled()) {
                            providerStart.set(false);
                        }

                    }, delay, TimeUnit.SECONDS);

                }


            }
        } finally {
            timer.shutdown();
            timer.shutdownNow();
            bitSwapRegistry.unregister(cid);
        }

        return Objects.requireNonNull(blockStore.getBlock(cid));
    }


    @Override
    @NonNull
    public Block getBlock(@NonNull Cancellable cancellable, @NonNull Cid cid)
            throws Exception {

        if (isClosed()) throw new IllegalStateException("Bitswap is closed");

        try {
            Block block = blockStore.getBlock(cid);
            if (block == null) {
                AtomicBoolean done = new AtomicBoolean(false);

                LogUtils.info(TAG, "Block Get " + cid);

                try {
                    return runWantHaves(() -> cancellable.isCancelled() || done.get() || isClosed(),
                            cid);
                } finally {
                    done.set(true);
                }
            }
            return block;

        } finally {
            LogUtils.info(TAG, "Block Release  " + cid);
        }
    }

    @Override
    public void receiveMessage(@NonNull Connection connection,
                               @NonNull MessageOuterClass.Message bsm) throws Exception {

        BitSwapMessage msg = BitSwapMessage.create(bsm);

        for (Block block : msg.blocks()) {
            Cid cid = block.cid();
            if (bitSwapRegistry.isRegistered(cid)) {

                LogUtils.info(TAG, "Received Block " + cid +
                        " " + connection.remotePeerId());

                blockStore.storeBlock(block);
                bitSwapRegistry.unregister(cid);
            }
        }

        for (Cid cid : msg.haves()) {
            if (bitSwapRegistry.isRegistered(cid)) {
                bitSwapRegistry.scheduleWants(connection, cid, new WantsTimerTask(cid, connection));
            }
        }

        bitSwapEngine.receiveMessage(connection, bsm);
    }

    private boolean isClosed() {
        return closed.get();
    }

    private record WantsTimerTask(Cid cid, Connection connection) implements Runnable {

        @Override
        public void run() {

            LogUtils.info(TAG, "Schedule Wants " +
                    cid + " " + connection.remotePeerId());

            writeMessage(connection, BitSwapMessage.create(
                            MessageOuterClass.Message.Wantlist.WantType.Block, cid),
                    IPFS.DHT_REQUEST_TIMEOUT)
                    .whenComplete((unused, throwable) -> {
                        if (throwable != null) {
                            LogUtils.error(TAG, throwable.getMessage());
                        }
                    });

        }
    }
}
