package threads.lite.cbor;

public interface Cborable {

    CborObject toCbor();

    @SuppressWarnings("unused")
    default byte[] serialize() {
        return toCbor().toByteArray();
    }
}
