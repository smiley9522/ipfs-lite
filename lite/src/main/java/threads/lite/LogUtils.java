package threads.lite;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Objects;


public interface LogUtils {

    @SuppressWarnings("SameReturnValue")
    static boolean isDebug() {
        return false;
    }

    @SuppressWarnings("SameReturnValue")
    static boolean isError() {
        return false;
    }

    static void verbose(@Nullable final String tag, @NonNull String message) {
        if (isDebug()) {
            Log.v(tag, message);
        }
    }

    static void warning(@Nullable final String tag, @NonNull String message) {
        if (isDebug()) {
            Log.w(tag, message);
        }
    }

    static void info(@Nullable final String tag, @NonNull String message) {
        if (isDebug()) {
            Log.i(tag, message);
        }
    }


    static void debug(@Nullable final String tag, @NonNull String message) {
        if (isDebug()) {
            Log.d(tag, message);
        }
    }

    static void error(@Nullable final String tag, @Nullable String message) {
        if (isError()) {
            Log.e(tag, Objects.requireNonNullElse(message, "No error message defined"));
        }
    }

    static void error(final String tag, @Nullable Throwable throwable) {
        if (isError()) {
            if (throwable != null) {
                Log.e(tag, throwable.getLocalizedMessage(), throwable);
            } else {
                Log.e(tag, "no throwable");
            }
        }
    }
}
