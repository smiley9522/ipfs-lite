package threads.lite;

import java.security.cert.X509Certificate;


public interface Utils {

    byte[] BYTES_EMPTY = new byte[0];
    X509Certificate[] CERTIFICATES_EMPTY = new X509Certificate[0];

    static byte[] concat(byte[]... chunks) throws IllegalStateException {
        int length = 0;
        for (byte[] chunk : chunks) {
            if (length > Integer.MAX_VALUE - chunk.length) {
                throw new IllegalStateException("exceeded size limit");
            }
            length += chunk.length;
        }
        byte[] res = new byte[length];
        int pos = 0;
        for (byte[] chunk : chunks) {
            System.arraycopy(chunk, 0, res, pos, chunk.length);
            pos += chunk.length;
        }
        return res;
    }

    static String bytesToHex(byte[] data, int length) {
        String digits = "0123456789abcdef";
        StringBuilder buffer = new StringBuilder();

        for (int i = 0; i != length; i++) {
            int v = data[i] & 0xff;

            buffer.append(digits.charAt(v >> 4));
            buffer.append(digits.charAt(v & 0xf));
        }

        return buffer.toString();
    }

    static String bytesToHex(byte[] data) {
        return bytesToHex(data, data.length);
    }

}
