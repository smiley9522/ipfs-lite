package threads.lite.cid;

import androidx.annotation.NonNull;

import com.google.protobuf.CodedInputStream;

import java.io.IOException;
import java.nio.ByteBuffer;

import threads.lite.core.DataHandler;


public record Prefix(Multicodec multicodec, Hashtype type, int version) {

    public static Prefix decode(byte[] bytes) throws IOException {
        CodedInputStream codedInputStream = CodedInputStream.newInstance(bytes);

        int version = codedInputStream.readRawVarint32();
        if (version != 1 && version != 0) {
            throw new IOException("invalid version");
        }

        int codec = codedInputStream.readRawVarint32();

        int mhtype = codedInputStream.readRawVarint32();

        int mhlen = codedInputStream.readRawVarint32();

        Hashtype hashType = Hashtype.lookup(mhtype);
        if (hashType.length() != mhlen) {
            throw new IOException("invalid length");
        }

        return new Prefix(Multicodec.get(codec), hashType, version);


    }

    @NonNull
    public static Cid sum(Prefix prefix, byte[] data) throws Exception {
        if (!prefix.isSha2556()) {
            throw new IOException("Type Multihash " +
                    prefix.type + " is not supported");
        }

        return switch (prefix.version) {
            case 0, 1 -> Cid.createCidV1(prefix.multicodec, data);
            default -> throw new Exception("Invalid cid version");
        };
    }

    private boolean isSha2556() {
        return type == Hashtype.SHA2_256;
    }

    public byte[] encoded() {
        int versionLength = DataHandler.unsignedVariantSize(version);
        int codecLength = DataHandler.unsignedVariantSize(multicodec.codec());
        int typeLength = DataHandler.unsignedVariantSize(type.index());
        int length = DataHandler.unsignedVariantSize(type.length());

        ByteBuffer buffer = ByteBuffer.allocate(versionLength + codecLength + typeLength + length);
        DataHandler.writeUnsignedVariant(buffer, version);
        DataHandler.writeUnsignedVariant(buffer, multicodec.codec());
        DataHandler.writeUnsignedVariant(buffer, type.index());
        DataHandler.writeUnsignedVariant(buffer, type.length());
        return buffer.array();
    }
}
