package threads.lite.cid;

import androidx.annotation.NonNull;

public record Multicodec(int codec) {
    public static final Multicodec IDENTITY = new Multicodec(0x00);
    public static final Multicodec RAW = new Multicodec(0x55);
    public static final Multicodec DagProtobuf = new Multicodec(0x70);
    public static final Multicodec Libp2pKey = new Multicodec(0x72);
    private static final Multicodec DagCbor = new Multicodec(0x71);

    @NonNull
    public static Multicodec get(int codec) {
        return switch (codec) {
            case 0x00 -> IDENTITY;
            case 0x55 -> RAW;
            case 0x70 -> DagProtobuf;
            case 0x71 -> DagCbor;
            case 0x72 -> Libp2pKey;
            default -> throw new IllegalArgumentException("unsupported multicodec " + codec);
        };
    }
}

