package threads.lite.holepunch;

import androidx.annotation.NonNull;

import java.net.DatagramSocket;
import java.util.function.Supplier;

import threads.lite.cid.IPV;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.quic.Parameters;

public record HolePunchInfo(@NonNull Supplier<IPV> ipv, @NonNull Multiaddr observed,
                            @NonNull DatagramSocket socket, @NonNull Parameters parameters) {

    @NonNull
    public PeerId getPeerId() {
        return observed.peerId();
    }
}
