package threads.lite.holepunch;

import java.util.Objects;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.PeerId;
import threads.lite.core.DataHandler;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Server;
import threads.lite.quic.Stream;
import threads.lite.quic.StreamRequester;

public final class HolePunchHandler implements ProtocolHandler {
    private final static String TAG = HolePunchHandler.class.getSimpleName();
    private final Server server;

    public HolePunchHandler(Server server) {
        this.server = server;
    }


    @Override
    public void protocol(Stream stream) {
        stream.writeOutput(DataHandler.encodeProtocols(IPFS.HOLE_PUNCH_PROTOCOL), false);
        HolePunch.initializeConnect(stream, server.getObserved());
    }

    @Override
    public void data(String alpn, Stream stream, byte[] data) throws Exception {
        PeerId peerId = Objects.requireNonNull((PeerId) stream.getAttribute(StreamRequester.PEER));
        HolePunch.sendSync(server, stream, peerId, data);
    }

    @Override
    public void fin(Stream stream) {
        LogUtils.error(TAG, "fin received " + stream.connection().remotePeerId());
    }

}
