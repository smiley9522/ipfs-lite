package threads.lite.store;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.TypeConverters;

import threads.lite.cid.PeerId;
import threads.lite.core.Page;


@Dao
public interface PageDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPage(Page page);

    @Query("SELECT * FROM Page WHERE peerId = :peerId")
    @TypeConverters(PeerId.class)
    Page getPage(PeerId peerId);

    @Query("UPDATE Page SET value =:value, sequence = sequence + 1, eol=:eol   WHERE peerId = :peerId")
    @TypeConverters(PeerId.class)
    void update(PeerId peerId, byte[] value, long eol);

    @Query("Select value From Page WHERE peerId = :peerId")
    @TypeConverters(PeerId.class)
    byte[] getValue(PeerId peerId);

}
