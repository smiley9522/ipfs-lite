package threads.lite.store;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import threads.lite.cid.Peer;

@Dao
public interface PeerDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPeer(Peer peer);

    @Query("SELECT * FROM Peer ORDER BY RANDOM() LIMIT :limit")
    List<Peer> getRandomPeers(int limit);

    @Delete
    void removePeer(Peer peer);
}
