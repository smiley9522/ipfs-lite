package threads.lite.store;

import androidx.room.RoomDatabase;

import threads.lite.core.Page;

@androidx.room.Database(entities = {Page.class}, version = 4, exportSchema = false)
public abstract class PageDatabase extends RoomDatabase {

    public abstract PageDao pageDao();

}
