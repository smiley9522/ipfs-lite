package threads.lite.store;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;

import java.util.List;

import threads.lite.cid.Peer;
import threads.lite.core.PeerStore;


public final class PEERS implements PeerStore {

    private static volatile PEERS INSTANCE = null;
    private final PeerDatabase bootstrapDatabase;

    private PEERS(PeerDatabase bootstrapDatabase) {
        this.bootstrapDatabase = bootstrapDatabase;
    }

    public static PEERS getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (PEERS.class) {
                if (INSTANCE == null) {
                    PeerDatabase bootstrapDatabase = Room.databaseBuilder(context, PeerDatabase.class,
                                    PeerDatabase.class.getSimpleName()).
                            allowMainThreadQueries().
                            fallbackToDestructiveMigration().build();

                    INSTANCE = new PEERS(bootstrapDatabase);
                }
            }
        }
        return INSTANCE;
    }

    @Override
    @NonNull
    public List<Peer> getRandomPeers(int limit) {
        return bootstrapDatabase.bootstrapDao().getRandomPeers(limit);
    }


    @Override
    public void storePeer(@NonNull Peer peer) {
        bootstrapDatabase.bootstrapDao().insertPeer(peer);
    }


    @Override
    public void removePeer(@NonNull Peer peer) {
        bootstrapDatabase.bootstrapDao().removePeer(peer);
    }

}
