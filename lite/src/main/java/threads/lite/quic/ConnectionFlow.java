package threads.lite.quic;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Delayed;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.function.IntUnaryOperator;
import java.util.function.LongUnaryOperator;

import threads.lite.LogUtils;
import threads.lite.tls.TlsEngine;


/**
 * This class implements the flow concepts of a QUIC connection
 * -> RttEstimator
 * <p>
 * -> CongestionController
 * <a href="https://datatracker.ietf.org/doc/html/rfc9002#name-congestion-control">...</a>
 * -> RecoveryManager
 * QUIC Loss Detection is specified in <a href="https://www.rfc-editor.org/rfc/rfc9002.html">...</a>.
 * <p>
 * "QUIC senders use acknowledgments to detect lost packets and a PTO to ensure acknowledgments are received"
 * It uses a single timer, because either there are lost packets to detect, or a probe must be scheduled, never both.
 * <p>
 * <b>Ack based loss detection</b>
 * When an Ack is received, packets that are sent "long enough" before the largest acked, are deemed lost; for the
 * packets not send "long enough", a timer is set to mark them as lost when "long enough" time has been passed.
 * <p>
 * An example:
 * -----------------------time------------------->>
 * sent:   1           2      3        4
 * acked:                                    4
 * \--- long enough before 4 --/                       => 1 is marked lost immediately
 * \--not long enough before 4 --/
 * |
 * Set timer at this point in time, as that will be "long enough".
 * At that time, a new timer will be set for 3, unless acked meanwhile.
 * <p>
 * <b>Detecting tail loss with probe timeout</b>
 * When no Acks arrive, no packets will be marked as lost. To trigger the peer to send an ack (so loss detection can do
 * its job again), a probe (ack-eliciting packet) will be sent after the probe timeout. If the situation does not change
 * (i.e. no Acks received), additional probes will be sent, but with an exponentially growing delay.
 * <p>
 * An example:
 * -----------------------time------------------->>
 * sent:   1           2      3        4
 * acked:                                    4
 * \-- timer set at loss time  --/
 * |
 * When the timer fires, there is no new ack received, so
 * nothing can be marked as lost. A probe is scheduled for
 * "probe timeout" time after the time 3 was sent:
 * \-- timer set at "probe timeout" time after 3 was sent --\
 * |
 * Send probe!
 * <p>
 * Note that packet 3 will not be marked as lost as long no ack is received!
 * <p>
 * <b>Exceptions</b>
 * Because a server might be blocked by the anti-amplification limit, a client must also send probes when it has no
 * ack eliciting packets in flight, but is not sure whether the peer has validated the client address.
 */
abstract class ConnectionFlow extends ConnectionSecrets {
    private static final String TAG = ConnectionFlow.class.getSimpleName();
    private static final int EPSILON = 3;
    private final SendRequestQueue[] sendRequestQueue =
            new SendRequestQueue[Level.LENGTH];
    private final PacketAssembler[] packetAssembler =
            new PacketAssembler[Level.LENGTH];
    private final AckGenerator[] ackGenerators =
            new AckGenerator[Level.LENGTH];
    private final AtomicBoolean[] discardedLevels =
            new AtomicBoolean[Level.LENGTH];
    private final CryptoStream[] cryptoStreams =
            new CryptoStream[Level.LENGTH];
    private final AtomicLong maxDataAssigned = new AtomicLong(0);
    private final int initialRtt; // todo find the default value in spec
    private final AtomicInteger rttVar = new AtomicInteger(Settings.NOT_DEFINED);
    private final AtomicInteger smoothedRtt = new AtomicInteger(Settings.NOT_DEFINED);
    private final AtomicInteger minRtt = new AtomicInteger(Integer.MAX_VALUE);
    private final AtomicInteger latestRtt = new AtomicInteger(0);
    private final AtomicLong bytesInFlight = new AtomicLong(0);
    private final AtomicLong congestionWindow = new AtomicLong(Settings.INITIAL_CONGESTION_WINDOW);
    private final ReduceCongestionWindowUpdater reduceCongestionWindowUpdater = new ReduceCongestionWindowUpdater();
    private final LossDetector[] lossDetectors = new LossDetector[Level.LENGTH];
    // TODO [Future urgent] replace by virtual threads
    private final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
    private final ReentrantLock scheduleLock = new ReentrantLock();
    private final AtomicInteger ptoCount = new AtomicInteger(0);
    private final AtomicLong timerExpiration = new AtomicLong(Settings.NOT_DEFINED);
    private final AtomicReference<HandshakeState> handshakeState =
            new AtomicReference<>(HandshakeState.Initial);

    // https://tools.ietf.org/html/draft-ietf-quic-transport-30#section-8.2
    // "If this value is absent, a default of 25 milliseconds is assumed."
    protected volatile int remoteMaxAckDelay = Settings.MAX_ACK_DELAY;
    // The maximum amount of data that can be sent (to the peer) on the connection as a whole
    private volatile long maxDataAllowed = 0L;
    // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-18.2
    // "initial_max_stream_data_bidi_local (0x0005):  This parameter is an integer value specifying the initial flow control limit for
    //  locally-initiated bidirectional streams.
    private volatile long initialMaxStreamDataBidiLocal = 0L;
    // "initial_max_stream_data_bidi_remote (0x0006):  This parameter is an integer value specifying the initial flow control limit for peer-
    //  initiated bidirectional streams. "
    private volatile long initialMaxStreamDataBidiRemote = 0L;
    // "initial_max_stream_data_uni (0x0007):  This parameter is an integer value specifying the initial flow control limit for unidirectional
    //  streams."
    private volatile long initialMaxStreamDataUni = 0L;
    private volatile long slowStartThreshold = Long.MAX_VALUE; // TODO [Future medium] what is the right value
    private volatile long congestionRecoveryStartTime = 0L;
    private ScheduledFuture<?> lossDetectionFuture;  // Concurrency: guarded by scheduleLock

    protected ConnectionFlow(Version version, Role role, int initialRtt) {
        super(version, role);
        this.initialRtt = initialRtt;


        for (Level level : Level.levels()) {
            sendRequestQueue[level.ordinal()] = new SendRequestQueue();
        }

        for (Level level : Level.levels()) {
            ackGenerators[level.ordinal()] = new AckGenerator();
        }

        discardedLevels[Level.Initial.ordinal()] = new AtomicBoolean(false);
        discardedLevels[Level.Handshake.ordinal()] = new AtomicBoolean(false);
        discardedLevels[Level.App.ordinal()] = new AtomicBoolean(false);


        for (Level level : Level.levels()) {
            int levelIndex = level.ordinal();
            packetAssembler[levelIndex] = new PacketAssembler(version, level,
                    sendRequestQueue[levelIndex], ackGenerators[levelIndex]);

        }

        for (Level level : Level.levels()) {
            lossDetectors[level.ordinal()] = new LossDetector(this);
        }

        this.lossDetectionFuture = new NullScheduledFuture();

    }

    protected void initializeCryptoStreams(TlsEngine tlsEngine) {
        for (Level level : Level.levels()) {
            cryptoStreams[level.ordinal()] = new CryptoStream(version, level,
                    role, tlsEngine, sendRequestQueue[level.ordinal()]);
        }
    }


    abstract void flush();


    final SendRequestQueue getSendRequestQueue(Level level) {
        return sendRequestQueue[level.ordinal()];
    }


    private PacketAssembler getPacketAssembler(Level level) {
        return packetAssembler[level.ordinal()];
    }

    /**
     * Assembles packets for sending in one datagram. The total size of the QUIC packets returned will never exceed
     * max packet size and for packets not containing probes, it will not exceed the remaining congestion window size.
     */
    final List<SendItem> assemble(int remainingCwndSize, byte[] sourceConnectionId, byte[] destinationConnectionId) {
        List<SendItem> packets = new ArrayList<>();
        int size = 0;

        int minPacketSize = 19 + destinationConnectionId.length;  // Computed for short header packet
        int remaining = Integer.min(remainingCwndSize, Settings.MAX_PACKAGE_SIZE);

        for (Level level : Level.levels()) {
            if (!isDiscarded(level)) {
                PacketAssembler assembler = getPacketAssembler(level);

                SendItem item = assembler.assemble(remaining,
                        Settings.MAX_PACKAGE_SIZE - size,
                        sourceConnectionId, destinationConnectionId);
                if (item != null) {
                    packets.add(item);
                    int packetSize = item.packet().estimateLength();
                    size += packetSize;
                    remaining -= packetSize;
                }
                if (remaining < minPacketSize && (Settings.MAX_PACKAGE_SIZE - size) < minPacketSize) {
                    // Trying a next level to produce a packet is useless
                    break;
                }

            }
        }


        return packets;
    }

    final int nextDelayedSendTime() {
        int nextDelayedSend = Settings.NOT_DEFINED;
        for (Level level : Level.levels()) {
            if (!isDiscarded(level)) {
                AckGenerator ackGenerator = ackGenerators[level.ordinal()];
                int delay = ackGenerator.nextDelayedSend();
                if (delay != Settings.NOT_DEFINED) {
                    if (nextDelayedSend == Settings.NOT_DEFINED) {
                        nextDelayedSend = delay;
                    } else {
                        nextDelayedSend = Math.min(nextDelayedSend, delay);
                    }
                }
            }
        }
        return nextDelayedSend;
    }


    final void addProbeRequest(Level level, Frame[] frames) {
        getSendRequestQueue(level).addProbeRequest(frames);
    }

    final void packetSent(Packet packet, long timeSent, int size, Consumer<Packet> packetLostCallback) {


        if (Packet.isInflightPacket(packet)) {
            PacketStatus packetStatus = new PacketStatus(packet, timeSent, size, packetLostCallback);
            registerInFlight(packetStatus);
            lossDetectors[packet.level().ordinal()].packetSent(packetStatus);
            setLossDetectionTimer();
        }

    }

    final void addRequest(Level level, Frame frame, Consumer<Frame> frameLostCallback) {
        getSendRequestQueue(level).addRequest(frame, frameLostCallback);
    }

    final void packetReceived(PacketReceived packetReceived, long timeReceived) {
        ackGenerators[packetReceived.level().ordinal()].packetReceived(packetReceived, timeReceived);
    }

    final void process(FrameReceived.AckFrame ackFrame, Level level, long timeReceived) {
        ackGenerators[level.ordinal()].ackFrameReceived(ackFrame);

        if (ptoCount.get() > 0) {
            // https://datatracker.ietf.org/doc/html/draft-ietf-quic-recovery-34#section-6.2.1
            // "To protect such a server from repeated client probes, the PTO backoff is not reset at a client that
            //  is not yet certain that the server has finished validating the client's address.
            if (peerAwaitingAddressValidation()) {
                LogUtils.verbose(TAG, "probe count not reset on ack because handshake not yet confirmed");
            } else {
                ptoCount.set(0);
            }
        }
        lossDetectors[level.ordinal()].processAckFrameReceived(ackFrame, timeReceived);

    }

    final void sendProbe(Frame[] frames, Level level) {
        if (!isDiscarded(level)) {
            addProbeRequest(level, frames);
            flush();
        }
    }

    /**
     * Stop sending packets, but don't shutdown yet, so connection close can be sent.
     */
    final void clearRequests() {
        // Stop sending packets, so discard any packet waiting to be send.
        for (SendRequestQueue queue : sendRequestQueue) {
            queue.clear();
        }

        // No more retransmissions either.
        stopRecovery();
    }

    void terminate() {
        for (Level level : Level.levels()) {
            discard(level);
        }
        discardKeys();

        for (Level level : Level.levels()) {
            cryptoStreams[level.ordinal()].cleanup();
        }
    }

    final CryptoStream getCryptoStream(Level level) {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-24#section-19.6
        // "There is a separate flow of cryptographic handshake data in each
        //   encryption level"
        return cryptoStreams[level.ordinal()];
    }

    final boolean isDiscarded(Level level) {
        return discardedLevels[level.ordinal()].get();
    }

    final void discard(Level level) {

        discardedLevels[level.ordinal()].set(true);

        // clear all send requests and probes on that level
        sendRequestQueue[level.ordinal()].clear();

        // 5.5.  Discarding Keys and Packet State
        //
        //   When packet protection keys are discarded (see Section 4.9 of
        //   [QUIC-TLS]), all packets that were sent with those keys can no longer
        //   be acknowledged because their acknowledgements cannot be processed
        //   anymore.  The sender MUST discard all recovery state associated with
        //   those packets and MUST remove them from the count of bytes in flight.
        lossDetectors[level.ordinal()].stop();


        // https://tools.ietf.org/html/draft-ietf-quic-recovery-33#section-6.2.2
        // "When Initial or Handshake keys are discarded, the PTO and loss detection timers MUST be reset"
        if (level == Level.Initial || level == Level.Handshake) {
            ptoCount.set(0);
            setLossDetectionTimer();
        }

        // deactivate ack generator for level
        ackGenerators[level.ordinal()].cleanup();
    }

    final int getPto() {
        return getSmoothedRtt() + 4 * getRttVar() + remoteMaxAckDelay;
    }


    final void init(long initialMaxData, long initialMaxStreamDataBidiLocal,
                    long initialMaxStreamDataBidiRemote, long initialMaxStreamDataUni) {
        this.initialMaxStreamDataBidiLocal = initialMaxStreamDataBidiLocal;
        this.initialMaxStreamDataBidiRemote = initialMaxStreamDataBidiRemote;
        this.initialMaxStreamDataUni = initialMaxStreamDataUni;
        this.maxDataAllowed = initialMaxData;
    }

    final void addMaxDataAssigned(long proposedStreamIncrement) {
        maxDataAssigned.getAndAdd(proposedStreamIncrement);
    }

    final long determineInitialMaxStreamData(Stream stream) {
        if (stream.isUnidirectional()) {
            return initialMaxStreamDataUni;
        } else if (role == Role.Client && stream.isClientInitiatedBidirectional()
                || role == Role.Server && stream.isServerInitiatedBidirectional()) {
            // For the receiver (imposing the limit) the stream is peer-initiated (remote).
            // "This limit applies to newly created bidirectional streams opened by the endpoint that receives
            // the transport parameter."
            return initialMaxStreamDataBidiRemote;
        } else if (role == Role.Client && stream.isServerInitiatedBidirectional()
                || role == Role.Server && stream.isClientInitiatedBidirectional()) {
            // For the receiver (imposing the limit), the stream is locally-initiated
            // "This limit applies to newly created bidirectional streams opened by the endpoint that sends the
            // transport parameter."
            return initialMaxStreamDataBidiLocal;
        } else {
            throw new IllegalStateException();
        }
    }

    final long getInitialMaxStreamDataBidiRemote() {
        return initialMaxStreamDataBidiRemote;
    }

    final void maxDataAllowed(long value) {
        maxDataAllowed = value;
    }

    /**
     * Returns the current connection flow control limit.
     *
     * @return current connection flow control limit
     */
    final long maxDataAllowed() {
        return maxDataAllowed;
    }

    final long maxDataAssigned() {
        return maxDataAssigned.get();
    }

    final void addSample(long timeReceived, long timeSent, int ackDelay) {
        if (timeReceived < timeSent) {
            // This sometimes happens in the Interop runner; reconsider solution after new sender is implemented.
            LogUtils.error(TAG, "Receiving negative rtt estimate: sent=" +
                    timeSent + ", received=" + timeReceived);
            return;
        }

        if (ackDelay > remoteMaxAckDelay) {
            ackDelay = remoteMaxAckDelay;
        }


        int rttSample = (int) (timeReceived - timeSent);
        if (rttSample < minRtt.get())
            minRtt.set(rttSample);
        // Adjust for ack delay if it's plausible. Because times are truncated at millisecond precision,
        // consider rtt equal to min as plausible.
        if (rttSample >= minRtt.get() + ackDelay) {
            rttSample -= ackDelay;
        }
        latestRtt.set(rttSample);

        if (smoothedRtt.get() == Settings.NOT_DEFINED) {
            // First time
            smoothedRtt.set(rttSample);
            rttVar.set(rttSample / 2);
        } else {
            int currentRttVar = Math.abs(smoothedRtt.get() - rttSample);
            rttVar.updateAndGet(new RttVarUpdater(currentRttVar));
            smoothedRtt.updateAndGet(new SmoothedRttUpdater(rttSample));
        }
    }

    final int getSmoothedRtt() {
        int val = smoothedRtt.get();
        if (val == Settings.NOT_DEFINED) {
            return initialRtt;
        } else {
            return val;
        }
    }

    final int getRttVar() {
        // Rtt-var is only used for computing PTO.
        // https://tools.ietf.org/html/draft-ietf-quic-recovery-23#section-5.3
        // "The initial probe timeout for a new connection or new path SHOULD be set to twice the initial RTT"
        // https://tools.ietf.org/html/draft-ietf-quic-recovery-23#section-5.2.1
        // "PTO = smoothed_rtt + max(4*rttvar, kGranularity) + max_ack_delay"
        // Hence, using an initial rtt-var of initial-rtt / 4, will result in an initial PTO of twice the initial RTT.
        // After the first packet is received, the rttVar will be computed from the real RTT sample.
        int val = rttVar.get();
        if (val == Settings.NOT_DEFINED) {
            return initialRtt / 4;
        } else {
            return val;
        }
    }

    final int getLatestRtt() {
        return latestRtt.get();
    }

    // the packet status is a packet send earlier and has now been acknowlegded
    final void processAckedPacket(PacketStatus acknowlegdedPacket) {


        boolean cwndLimited = congestionWindow.get() - bytesInFlight.get() <= EPSILON;

        bytesInFlight.updateAndGet(new BytesInFlightDiscard(acknowlegdedPacket.size()));


        // https://datatracker.ietf.org/doc/html/rfc9002#name-underutilizing-the-congesti
        // 7.8. Underutilizing the Congestion Window
        // When bytes in flight is smaller than the congestion window and sending is not pacing
        // limited, the congestion window is underutilized. This can happen due to insufficient
        // application data or flow control limits. When this occurs, the congestion window
        // SHOULD NOT be increased in either slow start or congestion avoidance.
        //

        // THIS is the correct implementation, but it is to slow
        // boolean underutilizingCongestionWindow = bytesInFlight.get() < congestionWindow.get();
        // if(!underutilizingCongestionWindow){


        // TODO [Future medium] make the right solution faster or analyse it more deeply
        if (cwndLimited) { // Note: this solution is faster, but not spec conform

            long congestionWindowCwnd = congestionWindow.get();

            // https://tools.ietf.org/html/draft-ietf-quic-recovery-23#section-6.4
            // "QUIC defines the end of recovery as a packet sent after the start of recovery being acknowledged"
            if (acknowlegdedPacket.timeSent() > congestionRecoveryStartTime) {
                congestionWindow.updateAndGet(
                        new IncreaseCongestionWindowUpdater(acknowlegdedPacket, slowStartThreshold));
            }

            if (congestionWindow.get() != congestionWindowCwnd) {
                LogUtils.info(TAG, "Cwnd(+): " + congestionWindow.get() + " (" + getMode()
                        + "); inflight: " + bytesInFlight.get());
            }
        }
    }

    final void discardBytesInFlight(PacketStatus packetStatus) {
        bytesInFlight.updateAndGet(new BytesInFlightDiscard(packetStatus.size()));
    }

    final void registerLost(PacketStatus packetStatus) {


        discardBytesInFlight(packetStatus);

        // 6.4.  Recovery Period
        //
        //   Recovery is a period of time beginning with detection of a lost
        //   packet or an increase in the ECN-CE counter.  Because QUIC does not
        //   retransmit packets, it defines the end of recovery as a packet sent
        //   after the start of recovery being acknowledged.  This is slightly
        //   different from TCP's definition of recovery, which ends when the lost
        //   packet that started recovery is acknowledged.
        //
        //   The recovery period limits congestion window reduction to once per
        //   round trip.  During recovery, the congestion window remains unchanged
        //   irrespective of new losses or increases in the ECN-CE counter.


        if (packetStatus.timeSent() > congestionRecoveryStartTime) {
            congestionRecoveryStartTime = System.currentTimeMillis();

            //   When a loss is detected,
            //   NewReno halves the congestion window and sets the slow start
            //   threshold  to the new congestion window.
            slowStartThreshold = congestionWindow.updateAndGet(reduceCongestionWindowUpdater);
        }
    }

    private Mode getMode() {
        if (congestionWindow.get() < slowStartThreshold) {
            return Mode.SlowStart;
        } else {
            return Mode.CongestionAvoidance;
        }
    }

    final long remainingCwnd() {
        // TODO [Future medium] this returns negative values
        return congestionWindow.get() - bytesInFlight.get();
    }

    final void registerInFlight(PacketStatus packetStatus) {
        bytesInFlight.addAndGet(packetStatus.size());

        if (bytesInFlight.get() > congestionWindow.get()) {
            LogUtils.info(TAG, "Bytes in flight exceeds congestion window: " +
                    bytesInFlight.get() + " > " + congestionWindow.get());
        }
    }

    final void setLossDetectionTimer() {
        LevelTime earliestLossTime = getEarliestLossTime();
        long lossTime = earliestLossTime != null ? earliestLossTime.lossTime : -1;
        if (lossTime > 0) {
            rescheduleLossDetectionTimeout(lossTime);
        } else {
            boolean ackElicitingInFlight = ackLostElicitingInFlight();
            boolean peerAwaitingAddressValidation = peerAwaitingAddressValidation();
            // https://datatracker.ietf.org/doc/html/draft-ietf-quic-recovery-34#section-6.2.2.1
            // "That is, the client MUST set the probe timer if the client has not received an acknowledgment for any of
            //  its Handshake packets and the handshake is not confirmed (...), even if there are no packets in flight."
            if (ackElicitingInFlight || peerAwaitingAddressValidation) {
                LevelTime ptoTimeAndSpace = getPtoLevelTime();
                if (ptoTimeAndSpace == null) {
                    LogUtils.verbose(TAG, "cancelling loss detection timer (no loss time set, no ack eliciting in flight, peer not awaiting address validation (1))");
                    unschedule();
                } else {
                    rescheduleLossDetectionTimeout(ptoTimeAndSpace.lossTime);

                    if (LogUtils.isDebug()) {
                        int timeout = (int) Math.abs(System.currentTimeMillis() - ptoTimeAndSpace.lossTime);
                        LogUtils.verbose(TAG, "reschedule loss detection timer for PTO over " + timeout + " millis, "
                                + "based on %s/" + ptoTimeAndSpace.level() + ", because "
                                + (peerAwaitingAddressValidation ? "peerAwaitingAddressValidation " : "")
                                + (ackElicitingInFlight ? "ackElicitingInFlight " : "")
                                + "| RTT:" + getSmoothedRtt() + "/" + getRttVar());
                    }
                }
            } else {
                LogUtils.verbose(TAG, "cancelling loss detection timer (no loss time set," +
                        " no ack eliciting in flight, peer not awaiting address validation (2))");
                unschedule();
            }
        }
    }

    /**
     * Determines the current probe timeout.
     * This method is defined in <a href="https://www.rfc-editor.org/rfc/rfc9002.html#name-setting-the-loss-detection-">...</a>.
     *
     * @return a <code>LevelTime</code> object defining the next probe: its time and for which packet number space.
     */
    private LevelTime getPtoLevelTime() {
        int ptoDuration = getSmoothedRtt() + Integer.max(1, 4 * getRttVar());
        ptoDuration *= (int) (Math.pow(2, ptoCount.get()));

        // The pseudo code in https://www.rfc-editor.org/rfc/rfc9002.html#name-setting-the-loss-detection- test for
        // ! ackElicitingInFlight() to determine whether peer is awaiting address validation. In a multi-threaded
        // implementation, that solution is subject to all kinds of race conditions, so its better to just check:
        HandshakeState handshakeStateCwnd = handshakeState.get();
        if (peerAwaitingAddressValidation()) {
            if (handshakeStateCwnd.hasNoHandshakeKeys()) {
                LogUtils.verbose(TAG, "getPtoTimeAndSpace: no ack eliciting in flight and no handshake keys -> probe Initial");
                return new LevelTime(Level.Initial, System.currentTimeMillis() + ptoDuration);
            } else {
                LogUtils.verbose(TAG, "getPtoTimeAndSpace: no ack eliciting in flight but handshake keys -> probe Handshake");
                return new LevelTime(Level.Handshake, System.currentTimeMillis() + ptoDuration);
            }
        }

        // Find earliest pto time
        long ptoTime = Long.MAX_VALUE;
        Level ptoSpace = null;
        for (Level level : Level.levels()) {
            if (lossDetectors[level.ordinal()].ackElicitingInFlight()) {
                if (level == Level.App && handshakeStateCwnd.isNotConfirmed()) {
                    // https://www.rfc-editor.org/rfc/rfc9002.html#name-setting-the-loss-detection-
                    // "Skip Application Data until handshake confirmed"
                    LogUtils.verbose(TAG, "getPtoTimeAndSpace is skipping level App, because handshake not yet confirmed!");
                    continue;  // Because App is the last, this is effectively a return.
                }
                if (level == Level.App) {
                    // https://www.rfc-editor.org/rfc/rfc9002.html#name-setting-the-loss-detection-
                    // "Include max_ack_delay and backoff for Application Data"
                    ptoDuration += remoteMaxAckDelay * (int) (Math.pow(2, ptoCount.get()));
                }
                long lastAckElicitingSent = lossDetectors[level.ordinal()].getLastAckElicitingSent();
                if (lastAckElicitingSent != Settings.NOT_DEFINED
                        && ((lastAckElicitingSent + ptoDuration) < ptoTime)) {
                    ptoTime = lastAckElicitingSent + ptoDuration;
                    ptoSpace = level;
                }
            }
        }

        if (ptoSpace != null) {
            return new LevelTime(ptoSpace, ptoTime);
        } else {
            return null;
        }
    }

    private boolean peerAwaitingAddressValidation() {
        return role == Role.Client && handshakeState.get().isNotConfirmed()
                && lossDetectors[Level.Handshake.ordinal()].noAckedReceived();
    }

    private void lossDetectionTimeout() {
        // Because cancelling the ScheduledExecutor task quite often fails, double check whether the timer should expire.
        long expiration = timerExpiration.get();
        long now = System.currentTimeMillis();
        if (expiration == Settings.NOT_DEFINED) {
            // Timer was cancelled, but it still fired; ignore
            LogUtils.warning(TAG, "Loss detection timeout: Timer was cancelled.");
            return;
        } else if (now < expiration && (expiration - now > 0)) {
            // Might be due to an old task that was cancelled, but unfortunately, it also happens that the scheduler
            // executes tasks much earlier than requested (30 ~ 40 ms). In that case, rescheduling is necessary to avoid
            // losing the loss detection timeout event.
            // To be sure the latest timer expiration is used, use timerExpiration i.s.o. the expiration of this call.
            LogUtils.warning(TAG, String.format("Loss detection timeout running (at %s) is %s ms too early; rescheduling to %s",
                    now, expiration - now, expiration));
            rescheduleLossDetectionTimeout(expiration);
        }

        LevelTime earliestLossTime = getEarliestLossTime();
        long lossTime = earliestLossTime != null ? earliestLossTime.lossTime : -1;
        if (lossTime > 0) {
            lossDetectors[earliestLossTime.level.ordinal()].detectLostPackets();
            flush();
            setLossDetectionTimer();
        } else {
            sendProbe();
            // Calling setLossDetectionTimer here not necessary, because the event of sending the probe will trigger it anyway.
            // And if done here, time of last-ack-eliciting might not be set yet (because packets are sent async), leading to trouble.
        }
    }

    private void sendProbe() {
        if (LogUtils.isDebug()) {
            LevelTime earliestLastAckElicitingSentTime = getLastAckElicitingSent();
            if (earliestLastAckElicitingSentTime != null) {
                LogUtils.verbose(TAG, String.format(Locale.US, "Sending probe %d, because no ack since %%s. Current RTT: %d/%d.",
                        ptoCount.get(), getSmoothedRtt(), getRttVar()));
            } else {
                LogUtils.verbose(TAG, String.format(Locale.US, "Sending probe %d. Current RTT: %d/%d.",
                        ptoCount.get(), getSmoothedRtt(), getRttVar()));
            }
        }

        int nrOfProbes = ptoCount.incrementAndGet() > 1 ? 2 : 1;

        if (ackLostElicitingInFlight()) {
            LevelTime ptoTimeAndSpace = getPtoLevelTime();
            if (ptoTimeAndSpace == null) {
                // So, the "ack eliciting in flight" has just been acked; a new timeout will be set, no need to send a probe now
                LogUtils.verbose(TAG, "Refraining from sending probe because received ack meanwhile");
                return;
            }
            sendOneOrTwoAckElicitingPackets(ptoTimeAndSpace.level, nrOfProbes);
        } else {
            // Must be the peer awaiting address validation or race condition
            if (peerAwaitingAddressValidation()) {
                LogUtils.verbose(TAG, "Sending probe because peer awaiting address validation");
                // https://tools.ietf.org/html/draft-ietf-quic-recovery-33#section-6.2.2.1
                // "When the PTO fires, the client MUST send a Handshake packet if it has Handshake keys, otherwise it
                //  MUST send an Initial packet in a UDP datagram with a payload of at least 1200 bytes."
                if (handshakeState.get().hasNoHandshakeKeys()) {
                    sendOneOrTwoAckElicitingPackets(Level.Initial, 1);
                } else {
                    sendOneOrTwoAckElicitingPackets(Level.Handshake, 1);
                }
            } else {
                LogUtils.verbose(TAG, "Refraining from sending probe as no ack eliciting in flight and no peer awaiting address validation");
            }
        }
    }

    private void sendOneOrTwoAckElicitingPackets(Level level, int numberOfPackets) {
        if (level == Level.Initial) {
            Frame[] framesToRetransmit = getFramesToRetransmit(level);
            if (framesToRetransmit.length == 0) {
                // This can happen, when the probe is sent because of peer awaiting address validation
                LogUtils.verbose(TAG, "(Probe is Initial ping, because there is no Initial data to retransmit)");
                for (int i = 0; i < numberOfPackets; i++) {
                    sendProbe(new Frame[]{Frame.PING, Frame.createPaddingFrame(2)}, Level.Initial);
                }
            } else {
                LogUtils.verbose(TAG, "(Probe is an initial retransmit)");
                for (int i = 0; i < numberOfPackets; i++) {
                    sendProbe(framesToRetransmit, Level.Initial);
                }
            }
        } else if (level == Level.Handshake) {
            // Client role: find ack eliciting handshake packet that is not acked and retransmit its contents.
            Frame[] framesToRetransmit = getFramesToRetransmit(level);
            if (framesToRetransmit.length == 0) {
                LogUtils.verbose(TAG, "(Probe is a handshake ping)");
                for (int i = 0; i < numberOfPackets; i++) {
                    sendProbe(new Frame[]{Frame.PING, Frame.createPaddingFrame(2)}, Level.Handshake);
                }
            } else {
                LogUtils.verbose(TAG, "(Probe is a handshake retransmit)");
                for (int i = 0; i < numberOfPackets; i++) {
                    sendProbe(framesToRetransmit, Level.Handshake);
                }
            }
        } else {
            Frame[] framesToRetransmit = getFramesToRetransmit(level);
            if (framesToRetransmit.length == 0) {
                LogUtils.verbose(TAG, ("(Probe is ping on level " + level + ")"));
                for (int i = 0; i < numberOfPackets; i++) {
                    sendProbe(new Frame[]{Frame.PING, Frame.createPaddingFrame(2)}, level);
                }
            } else {
                LogUtils.verbose(TAG, ("(Probe is retransmit on level " + level + ")"));
                for (int i = 0; i < numberOfPackets; i++) {
                    sendProbe(framesToRetransmit, level);
                }
            }
        }
    }

    private Frame[] getFramesToRetransmit(Level level) {
        List<Packet> unAckedPackets = lossDetectors[level.ordinal()].unAcked();

        List<FrameType> frameTypes = List.of(FrameType.PingFrame, FrameType.PaddingFrame, FrameType.AckFrame);
        for (Packet packet : unAckedPackets) {
            if (Packet.isAckEliciting(packet)) {
                if (!Packet.matchAll(packet, frameTypes)) {
                    // now have found first
                    List<Frame> result = new ArrayList<>();
                    for (Frame frame : packet.frames()) {
                        if (frame.frameType() != FrameType.AckFrame) {
                            result.add(frame);
                        }
                    }
                    Frame[] frames = new Frame[result.size()];
                    return result.toArray(frames);
                }
            }
        }
        return Settings.FRAMES_EMPTY;
    }

    private LevelTime getLastAckElicitingSent() {
        LevelTime earliestLossTime = null;
        for (Level level : Level.levels()) {
            long pnSpaceLossTime = lossDetectors[level.ordinal()].getLastAckElicitingSent();
            if (pnSpaceLossTime != Settings.NOT_DEFINED) {
                if (earliestLossTime == null) {
                    earliestLossTime = new LevelTime(level, pnSpaceLossTime);
                } else {
                    if (!(earliestLossTime.lossTime < pnSpaceLossTime)) {
                        earliestLossTime = new LevelTime(level, pnSpaceLossTime);
                    }
                }
            }
        }
        return earliestLossTime;
    }

    private LevelTime getEarliestLossTime() {
        LevelTime earliestLossTime = null;
        for (Level level : Level.levels()) {
            long pnSpaceLossTime = lossDetectors[level.ordinal()].getLossTime();
            if (pnSpaceLossTime != Settings.NOT_DEFINED) {
                if (earliestLossTime == null) {
                    earliestLossTime = new LevelTime(level, pnSpaceLossTime);
                } else {
                    if (!(earliestLossTime.lossTime < pnSpaceLossTime)) {
                        earliestLossTime = new LevelTime(level, pnSpaceLossTime);
                    }
                }
            }
        }
        return earliestLossTime;
    }

    private void rescheduleLossDetectionTimeout(long scheduledTime) {

        scheduleLock.lock();
        try {
            // Cancelling the current future and setting the new must be in a sync'd block
            // to ensure the right future is cancelled
            lossDetectionFuture.cancel(false);
            timerExpiration.set(scheduledTime);
            long delay = scheduledTime - System.currentTimeMillis();
            // Delay can be 0 or negative, but that's no problem for ScheduledExecutorService:
            // "Zero and negative delays are also allowed, and are treated as requests for immediate execution."
            if (!scheduler.isShutdown()) {
                lossDetectionFuture = scheduler.schedule(new LossDetectionRunnable(),
                        delay, TimeUnit.MILLISECONDS);
            }
        } catch (Throwable ignore) {
            // Can happen if has been reset concurrently
        } finally {
            scheduleLock.unlock();
        }
    }

    private void unschedule() {
        lossDetectionFuture.cancel(true);
        timerExpiration.set(Settings.NOT_DEFINED);
    }


    private boolean ackLostElicitingInFlight() {
        for (LossDetector lossDetector : lossDetectors) {
            if (lossDetector.ackElicitingInFlight()) {
                return true;
            }
        }
        return false;
    }

    final void stopRecovery() {
        unschedule();
        scheduler.shutdown();
        for (LossDetector lossDetector : lossDetectors) {
            lossDetector.stop();
        }
        scheduler.shutdownNow();
    }


    final void handshakeStateChangedEvent(HandshakeState newState) {

        HandshakeState oldState = handshakeState.getAndSet(newState);
        if (newState == HandshakeState.Confirmed && oldState != HandshakeState.Confirmed) {
            LogUtils.verbose(TAG, "State is set to " + newState);
            // https://tools.ietf.org/html/draft-ietf-quic-recovery-30#section-6.2.1
            // "A sender SHOULD restart its PTO timer (...), when the handshake is confirmed (...),"
            setLossDetectionTimer();
        }

    }


    enum Mode {
        // 7.3.1. Slow Start
        // A NewReno sender is in slow start any time the congestion window is below the slow start
        // threshold. A sender begins in slow start because the slow start threshold is initialized
        // to an infinite value.
        //
        // While a sender is in slow start, the congestion window increases by the number
        // of bytes acknowledged when each acknowledgment is processed. This results in
        // exponential growth of the congestion window.
        //
        // The sender MUST exit slow start and enter a recovery period when a packet is lost
        // or when the ECN-CE count reported by its peer increases.
        //
        // A sender reenters slow start any time the congestion window is less than the
        // slow start threshold, which only occurs after persistent congestion is declared.
        SlowStart,
        // 7.3.3. Congestion Avoidance
        // A NewReno sender is in congestion avoidance any time the congestion window
        // is at or above the slow start threshold and not in a recovery period.
        //
        // A sender in congestion avoidance uses an Additive Increase Multiplicative
        // Decrease (AIMD) approach that MUST limit the increase to the congestion window to
        // at most one maximum datagram size for each congestion window that is acknowledged.
        //
        // The sender exits congestion avoidance and enters a recovery period when a packet
        // is lost or when the ECN-CE count reported by its peer increases.
        CongestionAvoidance
    }

    private record RttVarUpdater(int currentRttVar) implements IntUnaryOperator {

        @Override
        public int applyAsInt(int operand) {
            // Add 2 to round to nearest integer
            return (3 * operand + currentRttVar + 2) / 4;
        }
    }

    private record SmoothedRttUpdater(int rrtSample) implements IntUnaryOperator {

        @Override
        public int applyAsInt(int operand) {
            // Add 4 to round to nearest integer
            return (7 * operand + rrtSample + 4) / 8;
        }
    }

    private static class ReduceCongestionWindowUpdater implements LongUnaryOperator {

        // When a loss is detected,
        // NewReno halves the congestion window and sets the slow start
        // threshold to the new congestion window.
        @Override
        public long applyAsLong(long operand) {
            long window = operand / Settings.CONGESTION_LOSS_REDUCTION_FACTOR;
            if (window < Settings.MINIMUM_CONGESTION_WINDOW) {
                return Settings.MINIMUM_CONGESTION_WINDOW;
            }
            return window;
        }
    }

    private record IncreaseCongestionWindowUpdater(
            PacketStatus packetStatus, long slowStartThreshold) implements LongUnaryOperator {

        @Override
        public long applyAsLong(long operand) {
            if (operand < slowStartThreshold) {
                // i.e. mode is slow start
                return operand + packetStatus.size(); // ok
            } else {
                // i.e. mode is congestion avoidance
                // A sender in congestion avoidance uses an Additive Increase Multiplicative
                // Decrease (AIMD) approach that MUST limit the increase to the congestion window to
                // at most one maximum datagram size for each congestion window that is acknowledged.
                // TODO [Future medium] understand the formula
                return operand + ((long) Settings.MAX_DATAGRAM_SIZE * packetStatus.size() / operand);
            }
        }
    }

    private record BytesInFlightDiscard(long discard) implements LongUnaryOperator {
        @Override
        public long applyAsLong(long operand) {
            long newValue = operand - discard;
            if (newValue < 0) { // note this is an implementation error
                if (LogUtils.isError()) {
                    throw new IllegalStateException("this is an implementation error");
                }
                return 0;
            }
            return newValue;
        }
    }

    private static class NullScheduledFuture implements ScheduledFuture<Void> {
        @Override
        public int compareTo(Delayed o) {
            return 0;
        }

        @Override
        public long getDelay(TimeUnit unit) {
            return 0;
        }

        @Override
        public boolean cancel(boolean mayInterruptIfRunning) {
            return false;
        }

        @Override
        public boolean isCancelled() {
            return false;
        }

        @Override
        public boolean isDone() {
            return false;
        }

        @Override
        public Void get() {
            return null;
        }

        @Override
        public Void get(long timeout, TimeUnit unit) {
            return null;
        }
    }

    private record LevelTime(Level level, long lossTime) {
    }

    private class LossDetectionRunnable implements Runnable {
        @Override
        public void run() {
            try {
                lossDetectionTimeout();
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }
    }
}
