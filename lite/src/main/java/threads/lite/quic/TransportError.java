package threads.lite.quic;


import androidx.annotation.NonNull;

import threads.lite.LogUtils;

// https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-20.1
public final class TransportError extends Exception {

    private final Code code;

    public TransportError(Code code, String message) {
        super(message);
        this.code = code;
        if (code != Code.NO_ERROR) {
            LogUtils.error(TransportError.class.getSimpleName(), toString());
        }
    }

    public String getTransportErrorMessage() {
        return getMessage();
    }

    public Code getTransportErrorCode() {
        return code;
    }

    @Override
    @NonNull
    public String toString() {
        return code.toString() + " " + getMessage();
    }


    // https://www.rfc-editor.org/rfc/rfc9000.html#name-transport-error-codes
    public enum Code {
        NO_ERROR(0x0),
        INTERNAL_ERROR(0x1),
        /**
         * @noinspection unused
         */
        CONNECTION_REFUSED(0x2),
        /**
         * @noinspection unused
         */
        FLOW_CONTROL_ERROR(0x3),
        STREAM_LIMIT_ERROR(0x4),
        STREAM_STATE_ERROR(0x5),
        /**
         * @noinspection unused
         */
        FINAL_SIZE_ERROR(0x6),
        FRAME_ENCODING_ERROR(0x7),
        TRANSPORT_PARAMETER_ERROR(0x8),
        CONNECTION_ID_LIMIT_ERROR(0x9),
        PROTOCOL_VIOLATION(0xa),
        /**
         * @noinspection unused
         */
        INVALID_TOKEN(0xb),
        /**
         * @noinspection unused
         */
        APPLICATION_ERROR(0xc),
        /**
         * @noinspection unused
         */
        CRYPTO_BUFFER_EXCEEDED(0xd),
        /**
         * @noinspection unused
         */
        KEY_UPDATE_ERROR(0xe),
        /**
         * @noinspection unused
         */
        AEAD_LIMIT_REACHED(0xf),
        /**
         * @noinspection unused
         */
        NO_VIABLE_PATH(0x10),

        CRYPTO_ERROR(0x100),
        // https://www.ietf.org/archive/id/draft-ietf-quic-version-negotiation-08.html#iana-error
        VERSION_NEGOTIATION_ERROR(0x53F8);   // !! When this document is approved, it will request permanent allocation of a codepoint in the 0-63 range to replace the provisional codepoint described above.


        private final short value;

        Code(int value) {
            this.value = (short) value;
        }

        public short value() {
            return value;
        }
    }
}
