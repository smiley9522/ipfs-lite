package threads.lite.quic;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.LongUnaryOperator;

final class LossDetector {

    private final ConnectionFlow connectionFlow;
    private final Map<Long, PacketStatus> packetSentLog = new ConcurrentHashMap<>();
    private final AtomicInteger ackElicitingInFlight = new AtomicInteger(0);
    private final AtomicLong largestAcked = new AtomicLong(-1);
    private volatile long lossTime = Settings.NOT_DEFINED;
    private volatile long lastAckElicitingSent = Settings.NOT_DEFINED;
    private volatile boolean isStopped = false;


    LossDetector(ConnectionFlow connectionFlow) {
        this.connectionFlow = connectionFlow;
    }

    void packetSent(PacketStatus packetStatus) {
        if (isStopped) {
            return;
        }
        if (Packet.isAckEliciting(packetStatus.packet())) {
            ackElicitingInFlight.incrementAndGet();
            lastAckElicitingSent = packetStatus.timeSent();
        }
        // During a reset operation, no new packets must be logged as sent.
        packetSentLog.put(packetStatus.packet().packetNumber(), packetStatus);

    }

    void processAckFrameReceived(FrameReceived.AckFrame ackFrame, long timeReceived) {
        if (isStopped) {
            return;
        }

        largestAcked.updateAndGet(new LargestAckedUpdater(ackFrame));


        PacketStatus newlyAcked = null;

        // it is ordered, packet status with highest packet number is at the top
        long[] acknowledgedRanges = ackFrame.acknowledgedRanges();
        for (int i = 0; i < acknowledgedRanges.length; i++) {
            long to = acknowledgedRanges[i];
            i++;
            long from = acknowledgedRanges[i];
            for (long pn = to; pn >= from; pn--) {
                PacketStatus packetStatus = packetSentLog.get(pn);
                if (packetStatus != null) {
                    if (!packetStatus.acked()) {
                        if (packetStatus.setAcked()) {
                            packetSentLog.remove(pn); // remove entry
                            if (Packet.isAckEliciting(packetStatus.packet())) {
                                ackElicitingInFlight.decrementAndGet();
                                connectionFlow.processAckedPacket(packetStatus);
                            }
                            if (newlyAcked == null) {
                                newlyAcked = packetStatus;
                            }
                        }
                    }
                }
            }
        }

        // https://tools.ietf.org/html/draft-ietf-quic-recovery-33#section-5.1
        // "An endpoint generates an RTT sample on receiving an ACK frame that meets the following two conditions:
        //   *  the largest acknowledged packet number is newly acknowledged, and
        //   *  at least one of the newly acknowledged packets was ack-eliciting."
        if (newlyAcked != null) {
            if (newlyAcked.packet().packetNumber() == ackFrame.largestAcknowledged()) {
                if (Packet.isAckEliciting(newlyAcked.packet())) { // at least one of the newly acknowledged packets was ack-eliciting."
                    connectionFlow.addSample(timeReceived, newlyAcked.timeSent(), ackFrame.ackDelay());
                }
            }
        }
        detectLostPackets();

        connectionFlow.setLossDetectionTimer();


    }

    public void stop() {
        isStopped = true;

        for (PacketStatus packetStatus : packetSentLog.values()) {
            if (packetStatus.inFlight()) {
                if (packetStatus.setLost()) {  // Only keep the ones that actually were set to lost
                    connectionFlow.discardBytesInFlight(packetStatus);
                }
            }
        }

        ackElicitingInFlight.set(0);
        packetSentLog.clear();
        lossTime = Settings.NOT_DEFINED;
        lastAckElicitingSent = Settings.NOT_DEFINED;
    }

    void detectLostPackets() {
        if (isStopped) {
            return;
        }

        float kTimeThreshold = 9f / 8f; // todo make a constant
        int lossDelay = (int) (kTimeThreshold * Integer.max(
                connectionFlow.getSmoothedRtt(), connectionFlow.getLatestRtt()));

        long lostSendTime = System.currentTimeMillis() - lossDelay;


        // https://tools.ietf.org/html/draft-ietf-quic-recovery-20#section-6.1
        // "A packet is declared lost if it meets all the following conditions:
        //   o  The packet is unacknowledged, in-flight, and was sent prior to an
        //      acknowledged packet.
        //   o  Either its packet number is kPacketThreshold smaller than an
        //      acknowledged packet (Section 6.1.1), or it was sent long enough in
        //      the past (Section 6.1.2)."
        // https://tools.ietf.org/html/draft-ietf-quic-recovery-20#section-2
        // "In-flight:  Packets are considered in-flight when they have been sent
        //      and neither acknowledged nor declared lost, and they are not ACK-
        //      only."


        long earliestSentTime = Settings.NOT_DEFINED;
        for (PacketStatus packetStatus : packetSentLog.values()) {
            if (packetStatus.inFlight()) {
                if (pnTooOld(packetStatus) || sentTimeTooLongAgo(packetStatus, lostSendTime)) {
                    if (!packetStatus.packet().isAckOnly()) {
                        declareLost(packetStatus);
                    }
                }

                // evaluate earliest time
                if (packetStatus.packet().packetNumber() <= largestAcked.get()) {
                    if (!packetStatus.packet().isAckOnly()) {
                        if (earliestSentTime == Settings.NOT_DEFINED) {
                            earliestSentTime = packetStatus.timeSent();
                        } else {
                            if (packetStatus.timeSent() < earliestSentTime) {
                                earliestSentTime = packetStatus.timeSent();
                            }
                        }
                    }
                }
            }
        }

        if (earliestSentTime != Settings.NOT_DEFINED && (earliestSentTime > lostSendTime)) {
            lossTime = earliestSentTime + lossDelay;
        } else {
            lossTime = Settings.NOT_DEFINED;
        }
    }

    long getLossTime() {
        return lossTime;
    }

    long getLastAckElicitingSent() {
        return lastAckElicitingSent;
    }

    boolean ackElicitingInFlight() {
        return ackElicitingInFlight.get() > 0;
    }

    List<Packet> unAcked() {
        List<Packet> unAcked = new ArrayList<>();
        for (PacketStatus packetStatus : packetSentLog.values()) {
            if (packetStatus.inFlight()) {
                if (!packetStatus.packet().isAckOnly()) {
                    unAcked.add(packetStatus.packet());
                }
            }
        }
        return unAcked;
    }

    private boolean pnTooOld(PacketStatus p) {
        int kPacketThreshold = 3;
        return p.packet().packetNumber() <= largestAcked.get() - kPacketThreshold;
    }

    private boolean sentTimeTooLongAgo(PacketStatus p, long lostSendTime) {
        return p.packet().packetNumber() <= largestAcked.get() && p.timeSent() < lostSendTime;
    }

    private void declareLost(PacketStatus packetStatus) {
        if (packetStatus.setLost()) { // Only keep the ones that actually were set to lost
            if (Packet.isAckEliciting(packetStatus.packet())) {
                ackElicitingInFlight.decrementAndGet();
                connectionFlow.registerLost(packetStatus);
            }
            // Retransmitting the frames in the lost packet is delegated to the lost frame callback, because
            // whether retransmitting the frame is necessary (and in which manner) depends on frame type,
            // see https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-13.3
            packetStatus.lostPacketCallback().accept(packetStatus.packet());

            packetSentLog.remove(packetStatus.packet().packetNumber());
        }
    }

    boolean noAckedReceived() {
        return largestAcked.get() < 0;
    }

    private record LargestAckedUpdater(
            FrameReceived.AckFrame ackFrame) implements LongUnaryOperator {
        @Override
        public long applyAsLong(long operand) {
            return Long.max(operand, ackFrame.largestAcknowledged());
        }
    }

}
