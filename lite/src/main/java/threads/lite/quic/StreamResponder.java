package threads.lite.quic;

public interface StreamResponder {

    void throwable(Throwable throwable);

    void protocol(Stream stream, String protocol) throws Exception;

    void data(Stream stream, String protocol, byte[] data) throws Exception;

    void fin(Stream stream, String protocol);
}
