package threads.lite.quic;


import androidx.annotation.NonNull;

import java.util.Objects;
import java.util.concurrent.TimeoutException;

import threads.lite.cid.Multiaddr;

public interface StreamRequester {
    String PEER = "PEER";
    String PROTOCOL = "PROTOCOL";

    @NonNull
    static Multiaddr remoteMultiaddr(@NonNull Connection connection) {
        return Multiaddr.create(connection.remotePeerId(), connection.remoteAddress());
    }

    @NonNull
    static Stream createStream(@NonNull Connection connection, @NonNull StreamRequester streamRequester)
            throws InterruptedException, TimeoutException {
        if (Objects.equals(connection.alpn(), Settings.LIBP2P_ALPN)) {
            return connection.createStream(AlpnLibp2pRequester.create(streamRequester),
                    true);
        }
        if (Objects.equals(connection.alpn(), Settings.LITE_ALPN)) {
            return connection.createStream(AlpnLiteRequester.create(streamRequester),
                    true);
        }
        throw new InterruptedException("not supported alpn");
    }

    @NonNull
    static Stream createStream(@NonNull Connection connection)
            throws InterruptedException, TimeoutException {
        return connection.createStream(true);
    }

    void throwable(Throwable throwable);

    void protocol(Stream stream, String protocol) throws Exception;

    void data(Stream stream, byte[] data) throws Exception;

    void terminated();

    void fin(Stream stream);

}
