package threads.lite.quic;

import java.util.function.Consumer;
import java.util.function.Function;

record SendRequest(int estimatedSize, Function<Integer, Frame> frameSupplier,
                   Consumer<Frame> lostCallback) {

}

