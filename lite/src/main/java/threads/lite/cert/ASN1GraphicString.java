package threads.lite.cert;

import java.io.IOException;

public abstract class ASN1GraphicString extends ASN1Primitive implements ASN1String {

    private final byte[] contents;

    ASN1GraphicString(byte[] contents) {
        if (null == contents) {
            throw new NullPointerException("'contents' cannot be null");
        }

        this.contents = contents;
    }

    static ASN1GraphicString createPrimitive(byte[] contents) {
        return new DERGraphicString(contents);
    }

    final boolean encodeConstructed() {
        return false;
    }

    final int encodedLength(boolean withTag) {
        return ASN1OutputStream.getLengthOfEncodingDL(withTag, contents.length);
    }

    final void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        out.writeEncodingDL(withTag, BERTags.GRAPHIC_STRING, contents);
    }

    final boolean asn1Equals(ASN1Primitive other) {
        if (!(other instanceof ASN1GraphicString that)) {
            return false;
        }

        return Arrays.areEqual(this.contents, that.contents);
    }

    public final String getString() {
        return Strings.fromByteArray(contents);
    }
}
