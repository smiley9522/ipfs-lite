package threads.lite.cert;

import androidx.annotation.NonNull;

import java.io.IOException;

/**
 * ASN.1 PrintableString object.
 * <p>
 * X.680 section 37.4 defines PrintableString character codes as ASCII subset of following characters:
 * </p>
 * <ul>
 * <li>Latin capital letters: 'A' .. 'Z'</li>
 * <li>Latin small letters: 'a' .. 'z'</li>
 * <li>Digits: '0'..'9'</li>
 * <li>Space</li>
 * <li>Apostrophe: '\''</li>
 * <li>Left parenthesis: '('</li>
 * <li>Right parenthesis: ')'</li>
 * <li>Plus sign: '+'</li>
 * <li>Comma: ','</li>
 * <li>Hyphen-minus: '-'</li>
 * <li>Full stop: '.'</li>
 * <li>Solidus: '/'</li>
 * <li>Colon: ':'</li>
 * <li>Equals sign: '='</li>
 * <li>Question mark: '?'</li>
 * </ul>
 * <p>
 * Explicit character set escape sequences are not allowed.
 * </p>
 */
public abstract class ASN1PrintableString extends ASN1Primitive implements ASN1String {
    private final byte[] contents;

    /**
     * Constructor with optional validation.
     *
     * @param string the base string to wrap.
     * @throws IllegalArgumentException if validate is true and the string
     *                                  contains characters that should not be in a PrintableString.
     */
    ASN1PrintableString(String string) {
        if (!isPrintableString(string)) {
            throw new IllegalArgumentException("string contains illegal characters");
        }

        this.contents = Strings.toByteArray(string);
    }

    ASN1PrintableString(byte[] contents) {
        this.contents = contents;
    }

    /**
     * return true if the passed in String can be represented without
     * loss as a PrintableString, false otherwise.
     *
     * @return true if in printable set, false otherwise.
     */
    private static boolean isPrintableString(
            String str) {
        for (int i = str.length() - 1; i >= 0; i--) {
            char ch = str.charAt(i);

            if (ch > 0x007f) {
                return false;
            }

            if ('a' <= ch && ch <= 'z') {
                continue;
            }

            if ('A' <= ch && ch <= 'Z') {
                continue;
            }

            if ('0' <= ch && ch <= '9') {
                continue;
            }

            switch (ch) {
                case ' ', '\'', '(', ')', '+', '-', '.', ':', '=', '?', '/', ',' -> {
                    continue;
                }
            }

            return false;
        }

        return true;
    }

    static ASN1PrintableString createPrimitive(byte[] contents) {
        return new DERPrintableString(contents);
    }

    public final String getString() {
        return Strings.fromByteArray(contents);
    }

    final boolean encodeConstructed() {
        return false;
    }

    final int encodedLength(boolean withTag) {
        return ASN1OutputStream.getLengthOfEncodingDL(withTag, contents.length);
    }

    final void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        out.writeEncodingDL(withTag, BERTags.PRINTABLE_STRING, contents);
    }

    final boolean asn1Equals(ASN1Primitive other) {
        if (!(other instanceof ASN1PrintableString that)) {
            return false;
        }

        return Arrays.areEqual(this.contents, that.contents);
    }


    @NonNull
    public String toString() {
        return getString();
    }
}
