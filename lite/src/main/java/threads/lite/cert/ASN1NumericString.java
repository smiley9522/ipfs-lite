package threads.lite.cert;

import androidx.annotation.NonNull;

import java.io.IOException;

/**
 * NumericString object - this is an ascii string of characters {0,1,2,3,4,5,6,7,8,9, }.
 * ASN.1 NUMERIC-STRING object.
 * <p>
 * This is an ASCII string of characters {0,1,2,3,4,5,6,7,8,9} + space.
 * <p>
 * See X.680 section 37.2.
 * <p>
 * Explicit character set escape sequences are not allowed.
 */
public abstract class ASN1NumericString extends ASN1Primitive implements ASN1String {
    private final byte[] contents;

    ASN1NumericString(byte[] contents) {
        this.contents = contents;
    }

    static ASN1NumericString createPrimitive(byte[] contents) {
        return new DERNumericString(contents);
    }

    public final String getString() {
        return Strings.fromByteArray(contents);
    }

    @NonNull
    public String toString() {
        return getString();
    }

    final boolean encodeConstructed() {
        return false;
    }

    final int encodedLength(boolean withTag) {
        return ASN1OutputStream.getLengthOfEncodingDL(withTag, contents.length);
    }

    final void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        out.writeEncodingDL(withTag, BERTags.NUMERIC_STRING, contents);
    }


    final boolean asn1Equals(ASN1Primitive other) {
        if (!(other instanceof ASN1NumericString that)) {
            return false;
        }

        return Arrays.areEqual(this.contents, that.contents);
    }
}
