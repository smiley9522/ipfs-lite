package threads.lite.cert;

import java.util.NoSuchElementException;


/**
 * General array utilities.
 */
public final class Arrays {
    private Arrays() {
        // static class, hide constructor
    }

    public static boolean areEqual(byte[] a, byte[] b) {
        return java.util.Arrays.equals(a, b);
    }

    public static boolean areEqual(char[] a, char[] b) {
        return java.util.Arrays.equals(a, b);
    }

    public static int hashCode(byte[] data) {

        if (data == null) {
            return 0;
        }

        int i = data.length;
        int hc = i + 1;

        while (--i >= 0) {
            hc *= 257;
            hc ^= data[i];
        }

        return hc;
    }

    public static byte[] clone(byte[] data) {
        return null == data ? null : data.clone();
    }

    public static byte[] prepend(byte[] a, byte b) {
        if (a == null) {
            return new byte[]{b};
        }

        int length = a.length;
        byte[] result = new byte[length + 1];
        System.arraycopy(a, 0, result, 1, length);
        result[0] = b;
        return result;
    }

    public static boolean isNullOrContainsNull(Object[] array) {
        if (null == array) {
            return true;
        }
        for (Object o : array) {
            if (null == o) {
                return true;
            }
        }
        return false;
    }

    /**
     * Iterator backed by a specific array.
     */
    public static class Iterator<T>
            implements java.util.Iterator<T> {
        private final T[] dataArray;

        private int position = 0;

        /**
         * Base constructor.
         * <p>
         * Note: the array is not cloned, changes to it will affect the values returned by next().
         * </p>
         *
         * @param dataArray array backing the iterator.
         */
        public Iterator(T[] dataArray) {
            this.dataArray = dataArray;
        }

        public boolean hasNext() {
            return position < dataArray.length;
        }

        public T next() {
            if (position == dataArray.length) {
                throw new NoSuchElementException("Out of elements: " + position);
            }

            return dataArray[position++];
        }

        public void remove() {
            throw new UnsupportedOperationException("Cannot remove element from an Array.");
        }
    }
}
