package threads.lite.cert;

/**
 * DER IA5String object - this is a ISO 646 (ASCII) string encoding code points 0 to 127.
 * <p>
 * Explicit character set escape sequences are not allowed.
 * </p>
 */
public final class DERIA5String extends ASN1IA5String {
    /**
     * Basic constructor - without validation.
     *
     * @param string the base string to use..
     */
    public DERIA5String(String string) {
        super(string);
    }


    DERIA5String(byte[] contents) {
        super(contents);
    }


}
