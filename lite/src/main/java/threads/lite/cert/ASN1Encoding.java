package threads.lite.cert;

/**
 * Supported encoding formats.
 */
public interface ASN1Encoding {
    /**
     * DER - distinguished encoding rules.
     */
    String DER = "DER";

    /**
     * DL - definite length encoding.
     */
    String DL = "DL";

}
