package threads.lite.dag;

import androidx.annotation.NonNull;

import java.util.concurrent.atomic.AtomicInteger;

import merkledag.pb.Merkledag;

public record DagStage(@NonNull Merkledag.PBNode node, @NonNull AtomicInteger index) {

    public static DagStage createDagStage(@NonNull Merkledag.PBNode node) {
        return new DagStage(node, new AtomicInteger(0));
    }

    public void incrementIndex() {
        index.incrementAndGet();
    }


    public void setIndex(int value) {
        index.set(value);
    }

    @NonNull
    @Override
    public String toString() {
        return node + " " + index.get() + " ";
    }
}
