package threads.lite.dag;


import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

import merkledag.pb.Merkledag;
import threads.lite.LogUtils;
import threads.lite.cid.Block;
import threads.lite.cid.Cid;
import threads.lite.cid.Dir;
import threads.lite.core.Cancellable;
import threads.lite.core.Link;
import threads.lite.core.ReaderInputStream;
import threads.lite.core.Session;
import unixfs.pb.Unixfs;


public interface DagStream {


    static boolean isDir(@NonNull Cancellable cancellable,
                         @NonNull Session session,
                         @NonNull Cid cid) throws Exception {

        Merkledag.PBNode node = DagResolver.resolveNode(session, cancellable, cid);
        Objects.requireNonNull(node);
        return DagService.isDirectory(node);
    }

    @NonNull
    static Dir createEmptyDirectory(@NonNull Session session) throws Exception {
        return DagService.createEmptyDirectory(session);
    }

    @NonNull
    static Dir addLinkToDirectory(@NonNull Session session,
                                  @NonNull Cid directory,
                                  @NonNull Link link) throws Exception {

        Block block = session.getBlockStore().getBlock(directory);
        Objects.requireNonNull(block, "Block not local available");
        Merkledag.PBNode dirNode = block.node();
        Objects.requireNonNull(dirNode);
        return DagService.addChild(session, dirNode, link);

    }

    @NonNull
    static Dir updateLinkToDirectory(@NonNull Session session, @NonNull Cid directory,
                                     @NonNull Link link) throws Exception {
        Block block = session.getBlockStore().getBlock(directory);
        Objects.requireNonNull(block, "Block not local available");
        Merkledag.PBNode dirNode = block.node();
        Objects.requireNonNull(dirNode);
        return DagService.updateChild(session, dirNode, link);

    }

    @NonNull
    static Dir createDirectory(@NonNull Session session, @NonNull List<Link> links)
            throws Exception {
        return DagService.createDirectory(session, links);
    }

    @NonNull
    static Dir removeFromDirectory(@NonNull Session session, @NonNull Cid directory,
                                   @NonNull String name) throws Exception {
        Block block = session.getBlockStore().getBlock(directory);
        Objects.requireNonNull(block, "Block not local available");
        Merkledag.PBNode dirNode = block.node();
        Objects.requireNonNull(dirNode);
        return DagService.removeChild(session, dirNode, name);
    }

    @NonNull
    static List<Cid> getBlocks(@NonNull Session session, @NonNull Cid cid) {
        List<Cid> result = new ArrayList<>();

        Block block = session.getBlockStore().getBlock(cid);
        if (block != null) { // block is locally available
            Merkledag.PBNode node = block.node();
            List<Merkledag.PBLink> links = node.getLinksList();

            for (Merkledag.PBLink link : links) {
                Cid child = Cid.decode(link.getHash().toByteArray());
                result.add(child);
                result.addAll(getBlocks(session, child));
            }
        }
        return result;
    }

    static void removeBlocks(@NonNull Session session, @NonNull Cid cid) {
        Block block = session.getBlockStore().getBlock(cid);
        if (block != null) { // block is locally available
            Merkledag.PBNode node = block.node();
            List<Merkledag.PBLink> links = node.getLinksList();

            for (Merkledag.PBLink link : links) {
                Cid child = Cid.decode(link.getHash().toByteArray());
                removeBlocks(session, child);
            }
            session.getBlockStore().deleteBlock(cid);
        }
    }

    static void ls(@NonNull Cancellable cancellable, @NonNull Consumer<Link> consumer,
                   @NonNull Session session, @NonNull Cid cid, boolean resolveChildren)
            throws Exception {

        Merkledag.PBNode node = DagResolver.resolveNode(session, cancellable, cid);
        Objects.requireNonNull(node);
        List<Merkledag.PBLink> links = node.getLinksList();
        for (Merkledag.PBLink link : links) {
            processLink(session, cancellable, consumer, link, resolveChildren);
        }
    }

    static boolean hasLink(@NonNull Cancellable cancellable,
                           @NonNull Session session, @NonNull Cid cid,
                           @NonNull String name) throws Exception {
        Merkledag.PBNode node = DagResolver.resolveNode(session, cancellable, cid);
        Objects.requireNonNull(node);
        Merkledag.PBLink link = DagReader.getLinkByName(node, name);
        return link != null;
    }

    @NonNull
    static Cid readInputStream(@NonNull Session session,
                               @NonNull ReaderInputStream readerInputStream) throws Exception {

        return DagService.createFromStream(session, readerInputStream);
    }

    private static void processLink(@NonNull Session session,
                                    @NonNull Cancellable cancellable,
                                    @NonNull Consumer<Link> consumer,
                                    @NonNull Merkledag.PBLink link,
                                    boolean resolveChildren) throws Exception {

        String name = link.getName();
        long size = link.getTsize();
        Cid cid = Cid.decode(link.getHash().toByteArray());

        if (resolveChildren) {

            Merkledag.PBNode linkNode = DagService.getNode(session, cancellable, cid);

            Unixfs.Data data = DagReader.getData(linkNode);

            int type;
            switch (data.getType()) {
                case File:
                    type = Link.File;
                    break;
                case Raw:
                    type = Link.Raw;
                    break;
                case Directory:
                    type = Link.Dir;
                    break;
                case Metadata:
                    LogUtils.error(DagStream.class.getSimpleName(),
                            "Handle MimeType" +
                                    Unixfs.Metadata.parseFrom(
                                            data.getData().toByteArray()).getMimeType());
                default:
                    type = Link.Unknown;
                    LogUtils.error(DagStream.class.getSimpleName(),
                            "TODO Handle " + data.getType().name());
            }
            size = data.getFilesize();
            consumer.accept(Link.create(cid, name, size, type));
        } else {
            consumer.accept(Link.create(cid, name, size, Link.Unknown));
        }
    }
}
