package threads.lite.dag;

import android.util.Pair;

import androidx.annotation.NonNull;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import merkledag.pb.Merkledag;
import threads.lite.cid.Cid;
import threads.lite.core.Cancellable;
import threads.lite.core.Session;


public interface DagResolver {

    @NonNull
    static Cid resolveNode(@NonNull Cancellable cancellable,
                           @NonNull Session session, @NonNull Cid root,
                           @NonNull List<String> path) throws Exception {

        Pair<Cid, List<String>> resolved = resolveToLastNode(session, cancellable, root, path);
        Cid cid = resolved.first;
        Objects.requireNonNull(cid);
        // make sure it is resolved
        Merkledag.PBNode res = resolveNode(session, cancellable, cid);
        Objects.requireNonNull(res);
        return cid;
    }

    @NonNull
    static Merkledag.PBNode resolveNode(@NonNull Session session,
                                        @NonNull Cancellable cancellable,
                                        @NonNull Cid cid) throws Exception {
        return DagService.getNode(session, cancellable, cid);
    }

    @NonNull
    private static Pair<Cid, List<String>> resolveToLastNode(@NonNull Session session,
                                                             @NonNull Cancellable cancellable,
                                                             @NonNull Cid root,
                                                             @NonNull List<String> path)
            throws Exception {

        if (path.size() == 0) {
            return Pair.create(root, Collections.emptyList());
        }

        Cid cid = root;
        Merkledag.PBNode node = DagService.getNode(session, cancellable, cid);

        for (String name : path) {
            Merkledag.PBLink lnk = DagReader.getLinkByName(node, name);
            Objects.requireNonNull(lnk, name + " not found");
            cid = Cid.decode(lnk.getHash().toByteArray());
            node = DagService.getNode(session, cancellable, cid);
        }

        return Pair.create(cid, Collections.emptyList());

    }

}
