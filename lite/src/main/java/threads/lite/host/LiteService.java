package threads.lite.host;

import androidx.annotation.NonNull;

import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import record.pb.EnvelopeOuterClass;
import threads.lite.IPFS;
import threads.lite.core.DataHandler;
import threads.lite.core.Page;
import threads.lite.ipns.IpnsService;
import threads.lite.quic.Connection;
import threads.lite.quic.Settings;
import threads.lite.quic.Stream;
import threads.lite.quic.StreamRequester;

public interface LiteService {

    @NonNull
    static Page pull(@NonNull Connection connection) throws Exception {

        if (Objects.equals(connection.alpn(), Settings.LITE_ALPN)) {
            // shortcut
            return IpnsService.createPage(IPFS.LITE_PULL_PROTOCOL,
                    StreamRequester.createStream(connection)
                            .request(DataHandler.createMessage(IPFS.LITE_PULL_PROTOCOL),
                                    IPFS.DEFAULT_REQUEST_TIMEOUT));
        } else {
            CompletableFuture<Page> done = new CompletableFuture<>();
            StreamRequester.createStream(connection, new PullRequester(done))
                    .request(DataHandler.encodeProtocols(IPFS.MULTISTREAM_PROTOCOL,
                            IPFS.LITE_PULL_PROTOCOL), IPFS.DEFAULT_REQUEST_TIMEOUT);
            return done.get(IPFS.DEFAULT_REQUEST_TIMEOUT, TimeUnit.SECONDS);
        }
    }


    static void push(@NonNull Connection connection,
                     @NonNull EnvelopeOuterClass.Envelope envelope) throws Exception {


        if (Objects.equals(connection.alpn(), Settings.LITE_ALPN)) {
            // shortcut
            StreamRequester.createStream(connection)
                    .request(DataHandler.createMessage(IPFS.LITE_PUSH_PROTOCOL, envelope),
                            IPFS.DEFAULT_REQUEST_TIMEOUT);
        } else {
            CompletableFuture<Void> done = new CompletableFuture<>();
            StreamRequester.createStream(connection, new PushRequester(done))
                    .request(DataHandler.encode(envelope, IPFS.MULTISTREAM_PROTOCOL,
                            IPFS.LITE_PUSH_PROTOCOL), IPFS.DEFAULT_REQUEST_TIMEOUT);
            done.get(IPFS.DEFAULT_REQUEST_TIMEOUT, TimeUnit.SECONDS);
        }

    }

    record PullRequester(CompletableFuture<Page> done) implements StreamRequester {
        @Override
        public void throwable(Throwable throwable) {
            done.completeExceptionally(throwable);
        }

        @Override
        public void protocol(Stream stream, String protocol) throws Exception {
            if (!Arrays.asList(IPFS.MULTISTREAM_PROTOCOL, IPFS.LITE_PULL_PROTOCOL).contains(protocol)) {
                throw new Exception("Token " + protocol + " not supported");
            }
        }

        @Override
        public void data(Stream stream, byte[] data) throws Exception {
            done.complete(IpnsService.createPage(IPFS.LITE_PULL_PROTOCOL, data));
        }

        @Override
        public void terminated() {
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream terminated"));
            }
        }

        @Override
        public void fin(Stream stream) {
            // this is invoked, that is good, but request is finished when message is parsed [ok]
        }

    }

    record PushRequester(CompletableFuture<Void> done) implements StreamRequester {
        @Override
        public void throwable(Throwable throwable) {
            done.completeExceptionally(throwable);
        }

        @Override
        public void protocol(Stream stream, String protocol) throws Exception {
            if (!Arrays.asList(IPFS.MULTISTREAM_PROTOCOL, IPFS.LITE_PUSH_PROTOCOL).contains(protocol)) {
                throw new Exception("Token " + protocol + " not supported");
            }
        }

        @Override
        public void data(Stream stream, byte[] data) throws Exception {
            throw new Exception("data method invoked [not expected]");
        }

        @Override
        public void terminated() {
            if (!done.isDone()) {
                done.completeExceptionally(new Throwable("stream terminated"));
            }
        }

        @Override
        public void fin(Stream stream) {
            done.complete(null);
            // this is invoked, that is good, but request is finished when message is parsed [ok]
        }

    }
}
