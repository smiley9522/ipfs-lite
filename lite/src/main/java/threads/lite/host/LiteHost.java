package threads.lite.host;


import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import java.net.DatagramSocket;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import crypto.pb.Crypto;
import ipns.pb.Ipns;
import record.pb.EnvelopeOuterClass;
import record.pb.RecordOuterClass;
import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.Utils;
import threads.lite.cid.Cid;
import threads.lite.cid.IPV;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multiaddrs;
import threads.lite.cid.Network;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.core.BlockStore;
import threads.lite.core.Keys;
import threads.lite.core.Page;
import threads.lite.core.PageStore;
import threads.lite.core.PeerInfo;
import threads.lite.core.PeerStore;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.core.SwarmStore;
import threads.lite.crypto.Key;
import threads.lite.ident.IdentityService;
import threads.lite.ipns.IpnsService;
import threads.lite.quic.ApplicationProtocolConnectionFactory;
import threads.lite.quic.Connection;
import threads.lite.quic.ServerConnection;
import threads.lite.quic.ServerConnector;
import threads.lite.quic.Settings;
import threads.lite.quic.TransportError;


public final class LiteHost {
    private static final String TAG = LiteHost.class.getSimpleName();

    @NonNull
    private final AtomicReference<IPV> version = new AtomicReference<>(IPV.IPv4v6);
    @NonNull
    private final BlockStore blockStore;
    @NonNull
    private final SwarmStore swarmStore;
    @NonNull
    private final PeerStore peerStore;
    @NonNull
    private final PageStore pageStore;
    @NonNull
    private final Keys keys;
    @NonNull
    private final PeerId self;
    @NonNull
    private final LiteCertificate liteCertificate;
    @NonNull
    private final Crypto.PublicKey publicKey;
    @NonNull
    private final Multiaddrs bootstrap = new Multiaddrs();
    @Nullable
    private Consumer<LitePush> incomingPush;
    @Nullable
    private Supplier<RecordOuterClass.Record> recordSupplier;

    public LiteHost(@NonNull Keys keys, @NonNull BlockStore blockStore,
                    @NonNull PeerStore peerStore, @NonNull SwarmStore swarmStore,
                    @NonNull PageStore pageStore)
            throws Exception {

        this.liteCertificate = LiteCertificate.createCertificate(keys);
        this.keys = keys;
        this.blockStore = blockStore;
        this.peerStore = peerStore;
        this.swarmStore = swarmStore;
        this.pageStore = pageStore;

        this.self = Key.createPeerId(keys.publicKey());
        evaluateInternetProtocol();


        this.publicKey = Key.createCryptoKey(keys.publicKey());

        for (String address : IPFS.DHT_BOOTSTRAP_NODES) {
            try {
                bootstrap.add(Multiaddr.create(address));
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }

        if (ipv().get() == IPV.IPv4 || ipv().get() == IPV.IPv4v6) {
            try {
                bootstrap.add(Multiaddr.create(IPFS.KAD_BOOTSTRAP));
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }

    }

    public static int nextFreePort() {
        int port = ThreadLocalRandom.current().nextInt(4001, 65535);
        if (isLocalPortFree(port)) {
            return port;
        } else {
            return nextFreePort();
        }
    }

    private static boolean isLocalPortFree(int port) {
        try {
            new ServerSocket(port).close();
            return true;
        } catch (Throwable ignore) {
            return false;
        }
    }

    @NonNull
    private static DatagramSocket getSocket(int port) {
        try {
            return new DatagramSocket(port);
        } catch (Throwable ignore) {
            return getSocket(nextFreePort());
        }
    }

    @NonNull
    private static IPV getProtocol(@NonNull List<InetAddress> addresses) {
        boolean ipv4 = false;
        boolean ipv6 = false;
        for (InetAddress inet : addresses) {
            if (inet instanceof Inet6Address) {
                ipv6 = true;
            } else {
                ipv4 = true;
            }
        }

        if (ipv4 && ipv6) {
            return IPV.IPv6; // prefer ipv6
        } else if (ipv4) {
            return IPV.IPv4;
        } else if (ipv6) {
            return IPV.IPv6;
        } else {
            return IPV.IPv4v6;
        }
    }

    @NonNull
    public PageStore getPageStore() {
        return pageStore;
    }


    public void setRecordSupplier(@Nullable Supplier<RecordOuterClass.Record> recordSupplier) {
        this.recordSupplier = recordSupplier;
    }

    @NonNull
    public Keys getKeys() {
        return keys;
    }

    @NonNull
    public Session createSession(@NonNull BlockStore blockStore,
                                 @NonNull Function<Cid, Boolean> isBitswapActive) {
        return new LiteSession(blockStore, this, isBitswapActive);
    }

    @NonNull
    public PeerId self() {
        return self;
    }

    @NonNull
    public BlockStore getBlockStore() {
        return blockStore;
    }

    @NonNull
    public Set<Peer> getRoutingPeers() {
        Set<Peer> peers = new HashSet<>();

        for (Multiaddr addr : bootstrap) {
            try {
                peers.add(Peer.create(addr, false));
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }
        return peers;
    }

    @NonNull
    SwarmStore getSwarmStore() {
        return swarmStore;
    }

    public void setIncomingPush(@Nullable Consumer<LitePush> incomingPush) {
        this.incomingPush = incomingPush;
    }


    public void updateNetwork() {
        evaluateInternetProtocol();
    }

    private void evaluateInternetProtocol() {
        List<InetAddress> addresses = new ArrayList<>();
        try {
            addresses.addAll(Network.networkAddresses());
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        if (addresses.isEmpty()) {
            version.set(IPV.IPv4);
        } else {
            version.set(getProtocol(addresses));
        }
    }

    void push(@NonNull Connection connection, @NonNull EnvelopeOuterClass.Envelope envelope) {
        // TODO [Future urgent] Should be invoked in a virtual thread
        try {
            if (incomingPush != null) {
                incomingPush.accept(new LitePush(connection, envelope));
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    @NonNull
    public PeerInfo getPeerInfo(@NonNull Connection connection) throws Exception {
        return IdentityService.getPeerInfo(self(), connection);
    }


    @NonNull
    public Multiaddrs getBootstrap() {
        return bootstrap;
    }


    @NonNull
    public Supplier<IPV> ipv() {
        return version::get;
    }


    // creates a self signed record (used for ipns)
    public byte[] createSelfSignedRecord(byte[] value, @NonNull Date eol,
                                         @Nullable String name) throws Exception {
        Duration duration = Duration.ofHours(IPFS.IPNS_DURATION);
        Ipns.IpnsRecord record =
                IpnsService.create(keys.privateKey(), value, eol, duration, name);
        return record.toByteArray();
    }

    @NonNull
    PeerStore getPeerStore() {
        return peerStore;
    }

    @NonNull
    RecordOuterClass.Record getIpnsRecord() throws Exception {
        if (recordSupplier != null) {
            return recordSupplier.get();
        }


        // return empty content
        @SuppressLint("SimpleDateFormat") String format = new SimpleDateFormat(
                IPFS.TIME_FORMAT_IPFS).format(new Date());
        byte[] sealedRecord = createSelfSignedRecord(Utils.BYTES_EMPTY, Page.getDefaultEol(), null);
        return RecordOuterClass.Record.newBuilder()
                .setKey(ByteString.copyFrom(Key.createIpnsKey(self())))
                .setValue(ByteString.copyFrom(sealedRecord))
                .setTimeReceived(format).build();

    }


    @NonNull
    private Server createServer(@NonNull DatagramSocket socket,
                                @NonNull Consumer<Connection> connectConsumer,
                                @NonNull Consumer<Connection> closedConsumer,
                                @NonNull Function<PeerId, Boolean> isGated) {


        LiteServer server = new LiteServer(this, socket);

        ServerConnector serverConnector = server.getServerConnector();

        serverConnector.setClosedConsumer(closedConsumer);
        ApplicationProtocolConnection protocolConnection =
                new ApplicationProtocolConnection(isGated, connectConsumer);

        serverConnector.registerApplicationProtocol(Settings.LIBP2P_ALPN, protocolConnection);

        serverConnector.registerApplicationProtocol(Settings.LITE_ALPN, protocolConnection);

        serverConnector.start();
        return server;
    }


    public Server startSever(int port,
                             @NonNull Consumer<Connection> connectConsumer,
                             @NonNull Consumer<Connection> closedConsumer,
                             @NonNull Function<PeerId, Boolean> isGated) {

        DatagramSocket socket = getSocket(port);

        return createServer(socket, connectConsumer, closedConsumer, isGated);

    }


    @NonNull
    LiteCertificate getLiteCertificate() {
        return liteCertificate;
    }

    @NonNull
    public Crypto.PublicKey getPublicKey() {
        return publicKey;
    }

    private static class ApplicationProtocolConnection extends ApplicationProtocolConnectionFactory {
        private final Function<PeerId, Boolean> isGated;
        private final Consumer<Connection> connectConsumer;

        public ApplicationProtocolConnection(
                Function<PeerId, Boolean> isGated, Consumer<Connection> connectConsumer) {
            this.isGated = isGated;
            this.connectConsumer = connectConsumer;
        }

        @Override
        public void createConnection(String protocol, ServerConnection connection) throws TransportError {
            PeerId remotePeerId;

            try {
                X509Certificate certificate = connection.remoteCertificate();
                Objects.requireNonNull(certificate);
                remotePeerId = LiteCertificate.extractPeerId(certificate);
                Objects.requireNonNull(remotePeerId);
            } catch (Throwable throwable) {
                throw new TransportError(TransportError.Code.APPLICATION_ERROR,
                        throwable.getMessage());
            }

            // now the remote PeerId is available
            connection.setRemotePeerId(remotePeerId);

            if (isGated.apply(remotePeerId)) {
                throw new TransportError(TransportError.Code.APPLICATION_ERROR,
                        "Peer is gated " + remotePeerId);
            }

            try {
                connectConsumer.accept(connection);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        }
    }

}


