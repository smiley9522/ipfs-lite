package threads.lite.host;

import androidx.annotation.NonNull;

import java.util.Objects;

import record.pb.RecordOuterClass;
import threads.lite.IPFS;
import threads.lite.core.DataHandler;
import threads.lite.core.ProtocolHandler;
import threads.lite.quic.Settings;
import threads.lite.quic.Stream;

public final class LitePullHandler implements ProtocolHandler {
    private final LiteHost host;

    LitePullHandler(@NonNull LiteHost host) {
        this.host = host;
    }

    @Override
    public void protocol(Stream stream) throws Exception {
        RecordOuterClass.Record response = host.getIpnsRecord();
        stream.writeOutput(DataHandler.encode(response, IPFS.LITE_PULL_PROTOCOL), true);
    }

    @Override
    public void data(String alpn, Stream stream, byte[] data) throws Exception {
        if (Objects.equals(alpn, Settings.LITE_ALPN)) {
            RecordOuterClass.Record response = host.getIpnsRecord();
            stream.response(response);
        } else {
            throw new Exception("should not be invoked");
        }
    }

    @Override
    public void fin(Stream stream) {
        // this is invoked, that is good, but request is finished when message is parsed [ok]
    }

}
