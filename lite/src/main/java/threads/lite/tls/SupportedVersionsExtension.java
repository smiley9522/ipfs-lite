/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package threads.lite.tls;

import java.nio.ByteBuffer;

/**
 * The TLS supported versions extension.
 * See <a href="https://tools.ietf.org/html/rfc8446#section-4.2.1">...</a>
 */
public record SupportedVersionsExtension(HandshakeType handshakeType,
                                         short tlsVersion) implements Extension {

    private static final short TLS13 = (short) 0x0304;

    public static SupportedVersionsExtension createSupportedVersionsExtension(HandshakeType handshakeType) {
        return new SupportedVersionsExtension(handshakeType, TLS13);
    }

    public static SupportedVersionsExtension parse(
            ByteBuffer buffer, HandshakeType handshakeType) throws ErrorAlert {
        short tlsVersion = 0;
        int extensionDataLength = Extension.parseExtensionHeader(
                buffer, TlsConstants.ExtensionType.supported_versions, 2);

        if (handshakeType == HandshakeType.client_hello) {
            int versionsLength = buffer.get() & 0xff;
            if (versionsLength % 2 == 0 && extensionDataLength == versionsLength + 1) {
                for (int i = 0; i < versionsLength; i += 2) {
                    short version = buffer.getShort();
                    // This implementation only supports TLS 1.3, so search for that version.
                    if (version == TLS13) {
                        tlsVersion = version;
                    }
                }
            } else {
                throw new DecodeErrorException("invalid versions length");
            }
        } else if (handshakeType == HandshakeType.server_hello) {
            if (extensionDataLength != 2) {
                throw new DecodeErrorException("Incorrect extension length");
            }
            tlsVersion = buffer.getShort();
        } else {
            throw new IllegalArgumentException();
        }
        // 0x0304 is TLS1.3
        if (tlsVersion != TLS13) throw new UnsupportedExtensionAlert("TLS1.3 not supported");
        return new SupportedVersionsExtension(handshakeType, TLS13);
    }

    public byte[] getBytes() {
        ByteBuffer buffer = ByteBuffer.allocate(handshakeType == HandshakeType.client_hello ? 7 : 6);
        buffer.putShort(TlsConstants.ExtensionType.supported_versions.value);

        // TLS 1.3
        if (handshakeType == HandshakeType.client_hello) {
            buffer.putShort((short) 3);  // Extension data length (in bytes)
            buffer.put((byte) 0x02);     // TLS versions bytes
        } else {
            buffer.putShort((short) 2);  // Extension data length (in bytes)
        }
        buffer.put(new byte[]{(byte) 0x03, (byte) 0x04});  // TLS 1.3

        return buffer.array();
    }
}
