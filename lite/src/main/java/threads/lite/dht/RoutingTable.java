package threads.lite.dht;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import threads.lite.IPFS;
import threads.lite.cid.ID;
import threads.lite.cid.Peer;

record RoutingTable(@NonNull Set<Peer> peers) {

    public List<QueryPeer> nearestPeers(@NonNull ID key) {

        ArrayList<QueryPeer> pds = new ArrayList<>();

        peers.forEach(peer -> pds.add(QueryPeer.create(peer, key)));

        // Sort by distance to target
        Collections.sort(pds);

        // now get the best result to limit 'IPFS.DHT_ALPHA'
        return pds.stream().limit(IPFS.DHT_ALPHA).collect(Collectors.toList());

    }

    public boolean addPeer(@NonNull Peer peer) {

        // peer already exists in the Routing Table.
        if (peers.contains(peer)) {
            return false;
        }

        // We have enough space in the table
        if (peers.size() < IPFS.DHT_TABLE_SIZE) {
            return peers.add(peer);
        }

        // try to add, but only when not a replaceable is found
        int index = new Random().nextInt(peers.size());
        Optional<Peer> optional = peers.stream().skip(index).findFirst();
        if (optional.isPresent()) {
            Peer random = optional.get();
            // let's evict it and add the new peer
            if (random.replaceable()) {
                if (removePeer(random)) {
                    return peers.add(peer);
                }
            }
        }
        return false;
    }

    public boolean removePeer(QueryPeer peer) {
        return removePeer(peer.peer());
    }


    private boolean removePeer(@NonNull Peer p) {
        if (p.replaceable()) {
            return peers.remove(p);
        }
        return false;
    }

    public void clear() {
        peers.clear();
    }

    public boolean isEmpty() {
        return peers.isEmpty();
    }

}