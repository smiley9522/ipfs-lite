package threads.lite.dht;

import androidx.annotation.NonNull;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.ID;
import threads.lite.cid.Peer;
import threads.lite.core.Cancellable;

interface Query {

    String TAG = Query.class.getSimpleName();

    private static List<QueryPeer> transform(@NonNull ID key, @NonNull List<Peer> peers) {
        List<QueryPeer> result = new ArrayList<>();
        peers.forEach(peer -> result.add(QueryPeer.create(peer, key)));
        return result;
    }

    static void runQuery(@NonNull DhtKademlia dht, @NonNull Cancellable cancellable,
                         @NonNull ID key, @NonNull List<QueryPeer> peers,
                         @NonNull DhtKademlia.QueryFunc queryFn) throws InterruptedException {

        // TODO [Future urgent] replace by virtual threads
        ExecutorService executorService = Executors.newFixedThreadPool(
                IPFS.DHT_ALPHA);
        try {
            ConcurrentSkipListSet<QueryPeer> queryPeers = new ConcurrentSkipListSet<>();
            enhanceSet(queryPeers, peers);

            iteration(dht, executorService, queryPeers, cancellable, key, queryFn);

            boolean termination = executorService.awaitTermination(
                    cancellable.timeout(), TimeUnit.SECONDS);
            LogUtils.info(TAG, "Termination " + termination);
        } finally {
            executorService.shutdown();
            executorService.shutdownNow();
        }
    }

    private static boolean enhanceSet(ConcurrentSkipListSet<QueryPeer> queryPeers,
                                      List<QueryPeer> peers) {
        // the peers in query update are added to the queryPeers
        boolean enhanceSet = false;
        for (QueryPeer peer : peers) {
            boolean result = queryPeers.add(peer);  // set initial state to PeerHeard
            if (result) {
                enhanceSet = true; // element was added
                /* TODO [Future low] limit the size of queryPeers
                if (queryPeers.size() > IPFS.DHT_TABLE_SIZE) {
                    QueryPeer last = queryPeers.pollLast();
                    if(last != null){
                        if(last != peer){
                            enhanceSet = true; // element was added
                        } else {
                           // LogUtils.error(TAG, "Added " + peer.toString());
                           // LogUtils.error(TAG, "Last" + last.toString());
                        }
                    }
                }*/
            }
        }
        return enhanceSet;
    }


    static void iteration(DhtKademlia dht, ExecutorService executorService,
                          ConcurrentSkipListSet<QueryPeer> queryPeers, Cancellable cancellable, ID key,
                          DhtKademlia.QueryFunc queryFn) throws InterruptedException {

        if (cancellable.isCancelled()) {
            executorService.shutdown();
            throw new InterruptedException("operation canceled");
        }

        List<QueryPeer> nextPeersToQuery = nextHeardPeers(queryPeers, IPFS.DHT_CONCURRENCY);

        for (QueryPeer queryPeer : nextPeersToQuery) {
            queryPeer.setState(PeerState.PeerWaiting);
            try {
                if (!executorService.isShutdown()) {
                    executorService.execute(() -> {
                        try {
                            List<Peer> newPeers = queryFn.query(cancellable, queryPeer.peer());
                            boolean enhancedSet = enhanceSet(queryPeers, transform(key, newPeers));
                            queryPeer.setState(PeerState.PeerQueried);
                            // query successful, try to add to routing table
                            if (enhancedSet) {
                                // only when the query peer is returning something
                                // it will be added to the routing table
                                dht.addToRouting(queryPeer);
                            }

                            iteration(dht, executorService, queryPeers, cancellable,
                                    key, queryFn);

                        } catch (InterruptedException ignore) {
                            executorService.shutdown();
                        } catch (ConnectException connectException) {
                            dht.removeFromRouting(queryPeer, false);
                            queryPeer.setState(PeerState.PeerUnreachable);
                        } catch (TimeoutException timeoutException) {
                            dht.removeFromRouting(queryPeer, true);
                            queryPeer.setState(PeerState.PeerUnreachable);
                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                            dht.removeFromRouting(queryPeer, true);
                            queryPeer.setState(PeerState.PeerUnreachable);
                        } finally {
                            if (isDone(queryPeers)) {
                                LogUtils.info(TAG, "Query peers done");
                                executorService.shutdown();
                            }
                        }
                    });
                }
            } catch (RejectedExecutionException ignore) {
                // standard exception
            }
        }

    }

    static boolean isDone(ConcurrentSkipListSet<QueryPeer> queryPeers) {
        for (QueryPeer queryPeer : queryPeers) {
            PeerState state = queryPeer.getState();
            if (state == PeerState.PeerHeard || state == PeerState.PeerWaiting) {
                return false;
            }
        }
        return true;
    }

    static List<QueryPeer> nextHeardPeers(ConcurrentSkipListSet<QueryPeer> queryPeers, int maxLength) {
        // The peers we query next should be ones that we have only Heard about.
        List<QueryPeer> peers = new ArrayList<>();
        int count = 0;
        for (QueryPeer queryPeer : queryPeers) {
            if (queryPeer.getState() == PeerState.PeerHeard) {
                peers.add(queryPeer);
                count++;
                if (count == maxLength) {
                    break;
                }
            }
        }
        return peers;

    }

}
