package threads.lite;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import junit.framework.TestCase;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Network;
import threads.lite.core.PeerInfo;
import threads.lite.core.Progress;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.mdns.MDNS;
import threads.lite.quic.Connection;
import threads.lite.quic.Parameters;
import threads.lite.quic.Settings;


@RunWith(AndroidJUnit4.class)
public class IpfsMdnsTest {

    private static final String TAG = IpfsMdnsTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    // This test only works when another mdns is online (probably kubo installation, daemon running in LAN)
    @Test
    public void mdns_test() throws Exception {

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }

        Server server = TestEnv.getServer();
        TestCase.assertNotNull(server);

        assertEquals(server.numServerConnections(), 0);

        MDNS serverMdns = IPFS.mdns(context);
        serverMdns.startService(server);

        try {
            CountDownLatch finishFirst = new CountDownLatch(1);

            long start = System.currentTimeMillis();
            MDNS mdns = IPFS.mdns(context);

            mdns.startDiscovery(peer -> {
                LogUtils.info(TAG, peer.toString());
                finishFirst.countDown();
            }); // not invoked in the test

            boolean finished = finishFirst.await(30, TimeUnit.SECONDS);

            LogUtils.info(TAG, "finished " + finished + " " +
                    (System.currentTimeMillis() - start) + "[ms]");

            assertTrue(finished);
            mdns.stop();

            Thread.sleep(3000); // 3 sec to recover

            CountDownLatch finishSecond = new CountDownLatch(1);

            start = System.currentTimeMillis();
            mdns = IPFS.mdns(context);

            mdns.startDiscovery(multiaddr -> {
                LogUtils.info(TAG, multiaddr.toString());
                finishSecond.countDown();
            }); // not invoked in the test


            finished = finishSecond.await(30, TimeUnit.SECONDS);

            LogUtils.info(TAG, "finished " + finished + " " +
                    (System.currentTimeMillis() - start) + "[ms]");


            assertTrue(finished);
            mdns.stop();
        } finally {
            serverMdns.stop();
        }

    }

    @Test
    public void mdns_test_access() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }

        Server server = TestEnv.getServer();
        TestCase.assertNotNull(server);

        assertEquals(server.numServerConnections(), 0);

        MDNS serverMdns = IPFS.mdns(context);
        serverMdns.startService(server);

        Dummy dummy = Dummy.getInstance(context);

        try {

            CountDownLatch finishFirst = new CountDownLatch(1);

            long start = System.currentTimeMillis();
            final AtomicReference<MDNS.Peer> reference = new AtomicReference<>(null);
            MDNS mdns = IPFS.mdns(context);

            mdns.startDiscovery(peer -> {
                LogUtils.info(TAG, peer.multiaddr().toString());
                reference.set(peer);
                finishFirst.countDown();
            });

            boolean finished = finishFirst.await(30, TimeUnit.SECONDS);

            LogUtils.info(TAG, "finished " + finished + " " +
                    (System.currentTimeMillis() - start) + "[ms]");

            assertTrue(finished);
            mdns.stop();

            MDNS.Peer peer = reference.get();
            assertNotNull(peer);
            Multiaddr multiaddr = peer.multiaddr();
            assertNotNull(multiaddr);

            String[] alpns = peer.alpns();
            assertNotNull(alpns);
            assertEquals(alpns.length, 2);
            assertTrue(List.of(alpns).contains(Settings.LIBP2P_ALPN));
            assertTrue(List.of(alpns).contains(Settings.LITE_ALPN));

            try (Session session = ipfs.createSession()) {

                byte[] input = TestEnv.getRandomBytes(10000000); // 10 MB

                Cid cid = IPFS.storeData(session, input);
                TestCase.assertNotNull(cid);

                try (Session dummySession = dummy.createSession()) {

                    Connection connection = dummySession.dial(multiaddr, Parameters.getDefault());
                    Objects.requireNonNull(connection);
                    TestCase.assertTrue(connection.isConnected());


                    PeerInfo peerInfo = ipfs.getPeerInfo(connection);
                    assertNotNull(peerInfo);
                    LogUtils.info(TAG, peerInfo.toString());

                    start = System.currentTimeMillis();
                    File file = TestEnv.createCacheFile(context);

                    //noinspection AnonymousInnerClassMayBeStatic,AnonymousInnerClass
                    IPFS.fetchToFile(session, file, cid, new Progress() {
                        @Override
                        public void setProgress(int progress) {
                            LogUtils.info(TAG, "Progress " + progress);
                        }

                        @Override
                        public boolean isCancelled() {
                            return false;
                        }
                    });


                    assertEquals(file.length(), input.length);

                    long end = System.currentTimeMillis();
                    LogUtils.info(TAG, "Time for downloading " + (end - start) / 1000 +
                            "[s]" + " " + file.length() / 1000 + " [KB]");
                    file.deleteOnExit();


                    connection.close();
                    Thread.sleep(1000);

                } finally {
                    dummy.clearDatabase();
                }

                Thread.sleep(3000);
                assertEquals(server.numServerConnections(), 0);
            }
        } finally {
            serverMdns.stop();
        }
    }
}
