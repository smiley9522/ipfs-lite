package threads.lite;

import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import junit.framework.TestCase;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import crypto.pb.Crypto;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Network;
import threads.lite.cid.Peer;
import threads.lite.cid.PeerId;
import threads.lite.core.Keys;
import threads.lite.core.Link;
import threads.lite.core.PeerInfo;
import threads.lite.core.Progress;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.core.TimeoutCancellable;
import threads.lite.crypto.Key;
import threads.lite.crypto.PubKey;


@RunWith(AndroidJUnit4.class)
public class IpfsTest {
    private static final String TAG = IpfsTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void peer_ids_test() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        //  -- Peer ID (sha256) encoded as a CID (inspect)
        PeerId peerId = IPFS.decodePeerId("bafzbeie5745rpv2m6tjyuugywy4d5ewrqgqqhfnf445he3omzpjbx5xqxe");
        assertNotNull(peerId);

        // -- Peer ID (sha256) encoded as a raw base58btc multihash
        peerId = IPFS.decodePeerId("QmYyQSo1c1Ym7orWxLYvCrM2EmxFTANf8wXmmE7DWjhx5N");
        assertNotNull(peerId);

        //  -- Peer ID (ed25519, using the "identity" multihash) encoded as a raw base58btc multihash
        peerId = IPFS.decodePeerId("12D3KooWD3eckifWpRn9wQpMG9R9hX3sD158z7EqHWmweQAJU5SA");
        assertNotNull(peerId);

        peerId = IPFS.decodePeerId(ipfs.self().toBase36());
        assertNotNull(peerId);
        assertEquals(peerId, ipfs.self());
    }


    @Test
    public void test_extractPublicKey() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);
        PubKey pubKey = Key.extractPublicKey(ipfs.self());
        assertNotNull(pubKey);
        assertEquals(pubKey.getKeyType(), Crypto.KeyType.Ed25519);

    }

    @Test
    public void test_keys() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);
        Keys keys = ipfs.getKeys();
        assertNotNull(keys.privateKey());
        assertNotNull(keys.publicKey());
    }

    @Test
    public void test_listenAddresses() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }

        Multiaddr[] result = IPFS.identity(server).multiaddrs();
        assertNotNull(result);
        for (Multiaddr ma : result) {
            LogUtils.debug(TAG, "Listen Address : " + ma.toString());
        }

        switch (server.getNatType()) {
            case UNKNOWN, SYMMETRIC -> TestCase.assertEquals(0, result.length);
            case RESTRICTED_CONE -> assertTrue(result.length >= 1);
        }

        PeerInfo info = IPFS.identity(server);
        Objects.requireNonNull(info);

        assertNotNull(info.peerId());
        assertFalse(info.hasRelayHop());
        assertEquals(info.agent(), IPFS.AGENT);
        assertEquals(info.peerId(), ipfs.self());

        String[] protocols = info.protocols();
        for (String protocol : protocols) {
            assertNotNull(protocol);
        }

        LogUtils.debug(TAG, info.toString());
    }


    @Test
    public void test_routing_peers() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);
        try (Session session = ipfs.createSession()) {
            Set<Peer> peers = session.getRoutingPeers();
            assertNotNull(peers);
            assertFalse(peers.isEmpty());

            for (Peer peer : peers) {
                LogUtils.warning(TAG, peer.toString());
            }
        }
    }

    @Test
    public void streamTest() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            String test = "Moin";
            Cid cid = IPFS.storeText(session, test);
            assertNotNull(cid);
            byte[] bytes = IPFS.getData(session, cid, () -> false);
            assertNotNull(bytes);
            assertEquals(test, new String(bytes));


            try {
                Cid fault = Cid.decode(ipfs.self().toString());
                IPFS.getData(session, fault, new TimeoutCancellable(10));
                fail();
            } catch (Exception ignore) {
                // ok
            }
        }

    }

    @Test
    public void textProgressTest() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            String test = "Moin bla bla";
            Cid cid = IPFS.storeText(session, test);
            assertNotNull(cid);

            AtomicInteger check = new AtomicInteger(0);

            //noinspection AnonymousInnerClassMayBeStatic,AnonymousInnerClass
            byte[] bytes = IPFS.getData(session, cid, new Progress() {
                @Override
                public void setProgress(int progress) {
                    check.set(progress);
                }

                @Override
                public boolean isCancelled() {
                    return false;
                }
            });
            assertNotNull(bytes);
            assertEquals(test, new String(bytes));


            //noinspection AnonymousInnerClassMayBeStatic,AnonymousInnerClass
            String text = IPFS.getText(session, cid, new Progress() {
                @Override
                public void setProgress(int progress) {
                    check.set(progress);
                }

                @Override
                public boolean isCancelled() {
                    return false;
                }
            });
            assertNotNull(text);
            assertEquals(test, text);

            assertEquals(100, check.get());

            text = IPFS.getText(session, cid, () -> false);
            assertNotNull(text);
            assertEquals(test, text);
        }

    }


    @Test(expected = Exception.class)
    public void textProgressAbortTest() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            String test = "Moin bla bla";
            Cid cid = IPFS.storeText(session, test);
            assertNotNull(cid);

            //noinspection AnonymousInnerClassMayBeStatic,AnonymousInnerClass
            IPFS.getData(session, cid, new Progress() {
                @Override
                public void setProgress(int progress) {
                    fail("should go here");
                }

                @Override
                public boolean isCancelled() {
                    return true;
                }
            });

            fail("expected fail");
        }

    }

    @Test
    public void test_timeout_cat() throws Exception {

        Cid notValid = Cid.decode("QmaFuc7VmzwT5MAx3EANZiVXRtuWtTwALjgaPcSsZ2Jdip");
        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            try {
                IPFS.getData(session, notValid, new TimeoutCancellable(10));
                fail();
            } catch (Exception ignore) {
                // ok
            }
        }
    }


    @Test
    public void test_add_cat() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            byte[] content = TestEnv.getRandomBytes(400000);

            Cid hash58Base = IPFS.storeData(session, content);
            assertNotNull(hash58Base);
            LogUtils.debug(TAG, hash58Base.toString());

            byte[] fileContents = IPFS.getData(session, hash58Base, () -> false);
            assertNotNull(fileContents);
            assertEquals(content.length, fileContents.length);
            assertEquals(new String(content), new String(fileContents));

            IPFS.removeBlocks(session, hash58Base);
        }
    }


    @Test(expected = Exception.class)
    public void test_ls_timeout() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            IPFS.links(session, Cid.decode("QmXm3f7uKuFKK3QUL1V1oJZnpJSYX8c3vdhd94evSQUPCH"),
                    true, new TimeoutCancellable(20));

        }
    }

    @Test
    public void test_ls_small() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        try (Session session = ipfs.createSession()) {
            Cid cid = IPFS.storeText(session, "hallo");
            assertNotNull(cid);
            List<Link> links = IPFS.links(session, cid, true, () -> false);
            assertNotNull(links);
            assertEquals(links.size(), 0);
            links = IPFS.links(session, cid, true, new TimeoutCancellable(20));
            assertNotNull(links);
            assertEquals(links.size(), 0);
        }
    }
}
