package threads.lite;


import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Set;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.Network;
import threads.lite.cid.Peer;
import threads.lite.core.PeerInfo;
import threads.lite.core.Session;
import threads.lite.ident.IdentityService;
import threads.lite.quic.Connection;


@RunWith(AndroidJUnit4.class)
public class IpfsBootstrapTest {
    private static final String TAG = IpfsBootstrapTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_bootstrap() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }

        Set<Multiaddr> bootstrap = ipfs.getBootstrap();
        assertNotNull(bootstrap);


        for (Multiaddr address : bootstrap) {
            LogUtils.error(TAG, "Bootstrap " + address.toString());
        }
        assertTrue(bootstrap.size() >= 4);

        Set<Peer> routingPeers = ipfs.getRoutingPeers();
        assertFalse(routingPeers.isEmpty());

        try (Session session = ipfs.createSession()) {
            for (Peer peer : routingPeers) {
                LogUtils.error(TAG, "Routing Peer " + peer.toString());

                Connection connection;
                try {
                    connection = IPFS.dial(session, peer.multiaddr(),
                            IPFS.getConnectionParameters());
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, "Connection failed " +
                            throwable.getClass().getSimpleName());
                    continue;
                }
                try {
                    assertNotNull(connection);
                    PeerInfo info = IdentityService.getPeerInfo(ipfs.self(), connection);
                    assertNotNull(info);
                    LogUtils.error(TAG, info.toString());

                    if (!info.hasProtocol(IPFS.DHT_PROTOCOL)) {
                        LogUtils.error(TAG, "Error has no DHT protocol !!!");
                    }

                } catch (Throwable throwable) {
                    LogUtils.error(TAG, "PeerInfo failed " +
                            throwable.getClass().getSimpleName());
                }
            }
        }
    }

}
