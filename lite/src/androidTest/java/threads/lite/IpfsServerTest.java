package threads.lite;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import record.pb.EnvelopeOuterClass;
import threads.lite.cid.Cid;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.cid.Record;
import threads.lite.core.PeerInfo;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.core.TimeoutCancellable;
import threads.lite.quic.Connection;
import threads.lite.quic.Parameters;


@RunWith(AndroidJUnit4.class)
public class IpfsServerTest {

    private static final String TAG = IpfsServerTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void server_test() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);

        assertEquals(server.numServerConnections(), 0);


        try (Session session = ipfs.createSession()) {
            String text = "Hallo das ist ein Test";
            Cid cid = IPFS.storeText(session, text);
            assertNotNull(cid);

            assertNotNull(session.isBitswapActive());

            Dummy dummy = Dummy.getInstance(context);
            Multiaddr multiaddr = Multiaddr.getLoopbackAddress(ipfs.self(), server.getPort());


            try (Session dummySession = dummy.createSession()) {

                PeerId host = ipfs.self();
                assertNotNull(host);

                Connection connection = dummySession.dial(multiaddr, Parameters.getDefault());
                Objects.requireNonNull(connection);


                PeerInfo info = dummy.getHost().getPeerInfo(connection);
                assertNotNull(info);
                assertEquals(info.agent(), IPFS.AGENT);

                // simple push test
                byte[] test = "moin".getBytes(StandardCharsets.UTF_8);
                EnvelopeOuterClass.Envelope data =
                        IPFS.createEnvelope(dummySession, Record.LITE, test);

                AtomicBoolean notified = new AtomicBoolean(false);
                ipfs.setIncomingPush(push -> {
                    EnvelopeOuterClass.Envelope envelope = push.envelope();
                    notified.set(Arrays.equals(envelope.getPayload().toByteArray(), test));
                });


                IPFS.push(connection, data);
                Thread.sleep(1000);
                assertTrue(notified.get());


                String cmpText = IPFS.getText(dummySession, cid, new TimeoutCancellable(10));
                assertEquals(text, cmpText);


                connection.close();
            } finally {
                dummy.clearDatabase();
            }
        }

    }


    @Test
    public void server_multiple_dummy_conn() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);
        try (Session session = ipfs.createSession()) {
            byte[] input = TestEnv.getRandomBytes(50000);

            Cid cid = IPFS.storeData(session, input);
            assertNotNull(cid);

            ExecutorService executors = Executors.newFixedThreadPool(
                    Runtime.getRuntime().availableProcessors());

            AtomicInteger finished = new AtomicInteger();
            int instances = 100;
            int timeSeconds = 200;
            for (int i = 0; i < instances; i++) {

                executors.execute(() -> {

                    try {
                        Dummy dummy = Dummy.getInstance(context);

                        Multiaddr multiaddr = Multiaddr.getLoopbackAddress(
                                ipfs.self(), server.getPort());


                        try (Session dummySession = dummy.createSession()) {

                            PeerId serverPeer = ipfs.self();
                            LogUtils.debug(TAG, "Server :" + serverPeer);
                            PeerId client = dummy.self();
                            LogUtils.debug(TAG, "Client :" + client);


                            Connection connection = dummySession.dial(multiaddr, Parameters.getDefault());
                            assertNotNull(connection);

                            PeerInfo info = dummy.getHost().getPeerInfo(connection);
                            assertNotNull(info);
                            assertEquals(info.agent(), IPFS.AGENT);


                            byte[] output = IPFS.getData(dummySession, cid,
                                    new TimeoutCancellable(timeSeconds));
                            assertArrayEquals(input, output);

                            connection.close();

                            LogUtils.info(TAG, "finished " + finished.incrementAndGet());
                        } finally {
                            dummy.clearDatabase();
                        }

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                        fail();
                    }

                });
            }

            executors.shutdown();

            assertTrue(executors.awaitTermination(timeSeconds, TimeUnit.SECONDS));
            assertEquals(finished.get(), instances);
        }

    }
}