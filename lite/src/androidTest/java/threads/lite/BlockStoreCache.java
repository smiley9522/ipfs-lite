package threads.lite;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;

import java.util.UUID;

import threads.lite.cid.Block;
import threads.lite.cid.Cid;
import threads.lite.core.BlockStore;
import threads.lite.store.BlockStoreDatabase;


public final class BlockStoreCache implements BlockStore, AutoCloseable {

    private static final String TAG = BlockStoreCache.class.getSimpleName();
    private final BlockStoreDatabase blocksStoreDatabase;
    private final String name;
    private final Context context;

    private BlockStoreCache(Context context, BlockStoreDatabase blocksStoreDatabase, String name) {
        this.context = context;
        this.blocksStoreDatabase = blocksStoreDatabase;
        this.name = name;
    }

    public static BlockStoreCache createInstance(@NonNull Context context) {

        UUID uuid = UUID.randomUUID();
        BlockStoreDatabase blocksStoreDatabase = Room.databaseBuilder(context,
                        BlockStoreDatabase.class,
                        uuid.toString()).
                allowMainThreadQueries().
                fallbackToDestructiveMigration().build();
        return new BlockStoreCache(context, blocksStoreDatabase, uuid.toString());

    }


    @Override
    public void clear() {
        blocksStoreDatabase.clearAllTables();
    }


    @Override
    public boolean hasBlock(@NonNull Cid cid) {
        return blocksStoreDatabase.blockDao().hasBlock(cid);
    }

    @Nullable
    @Override
    public Block getBlock(@NonNull Cid cid) {
        return blocksStoreDatabase.blockDao().getBlock(cid);
    }

    @Nullable
    @Override
    public byte[] getBlockData(@NonNull Cid cid) {
        return blocksStoreDatabase.blockDao().getData(cid);
    }

    @Override
    public void storeBlock(@NonNull Block block) {
        blocksStoreDatabase.blockDao().insertBlock(block);
    }

    @Override
    public void deleteBlock(@NonNull Cid cid) {
        blocksStoreDatabase.blockDao().deleteBlock(cid);
    }

    @NonNull
    public String getName() {
        return name;
    }

    @Override
    public void close() {
        clear();
        boolean success = context.deleteDatabase(name);
        LogUtils.info(TAG, "Delete success " + success + " of database " + name);

    }
}
