package threads.lite;


import static org.junit.Assert.assertFalse;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.Network;
import threads.lite.core.Session;
import threads.lite.core.TimeoutCancellable;

@RunWith(AndroidJUnit4.class)
public class IpfsFindClosestPeersTest {
    private static final String TAG = IpfsFindClosestPeersTest.class.getSimpleName();


    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void find_closest_peers() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }

        Set<Multiaddr> found = ConcurrentHashMap.newKeySet();

        try (Session session = ipfs.createSession()) {

            IPFS.findClosestPeers(session, ipfs.self(), found::add,
                    new TimeoutCancellable(() -> found.size() > 10, 30));

            for (Multiaddr multiaddr : found) {
                LogUtils.error(TAG, multiaddr.toString());
            }

            assertFalse(found.isEmpty());
        }
    }
}
