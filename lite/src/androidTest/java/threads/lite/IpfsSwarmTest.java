package threads.lite;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import junit.framework.TestCase;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.cid.Network;
import threads.lite.core.NatType;
import threads.lite.core.Page;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.core.TimeoutCancellable;
import threads.lite.host.LiteService;
import threads.lite.quic.Connection;

@RunWith(AndroidJUnit4.class)
public class IpfsSwarmTest {

    private static final String TAG = IpfsSwarmTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void test_find_peer_in_swarm() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        TestCase.assertNotNull(server);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }

        if (server.getNatType() != NatType.RESTRICTED_CONE) {
            LogUtils.warning(TAG, "nothing to test here NAT TYPE is not RESTRICTED_CONE");
            return;
        }

        assertFalse(IPFS.reservations(server).isEmpty()); // pre-condition (reservations done on swarm)

        // now we check if we can find ourself
        AtomicBoolean found = new AtomicBoolean(false);
        Dummy dummy = Dummy.getInstance(context);
        try (Session dummySession = dummy.createSession()) {
            dummySession.findPeer(new TimeoutCancellable(120), multiaddr -> {
                LogUtils.error(TAG, multiaddr.toString());
                found.set(true);
            }, ipfs.self()); // find me
        }

        assertTrue(found.get());

    }

    @Test
    public void test_dial_peer_in_swarm() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        TestCase.assertNotNull(server);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }

        if (server.getNatType() != NatType.RESTRICTED_CONE) {
            LogUtils.warning(TAG, "nothing to test here NAT TYPE is not RESTRICTED_CONE");
            return;
        }

        assertFalse(IPFS.reservations(server).isEmpty()); // pre-condition (reservations done on swarm)

        // now we check if we can dial ourself
        Dummy dummy = Dummy.getInstance(context);
        try (Session dummySession = dummy.createSession()) {

            Connection conn = dummySession.dial(new TimeoutCancellable(120),
                    ipfs.self(), IPFS.getConnectionParameters()); // find me

            assertNotNull(conn);

            Page ipnsEntry = LiteService.pull(conn);
            assertNotNull(ipnsEntry);

            LogUtils.error(TAG, ipnsEntry.toString());
        }


    }

    @Test
    public void test_resolve_peer_in_swarm() throws Exception {
        IPFS ipfs = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        TestCase.assertNotNull(server);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }

        if (server.getNatType() != NatType.RESTRICTED_CONE) {
            LogUtils.warning(TAG, "nothing to test here NAT TYPE is not RESTRICTED_CONE");
            return;
        }

        assertFalse(IPFS.reservations(server).isEmpty()); // pre-condition (reservations done on swarm)

        // now we check if we can dial ourself
        Dummy dummy = Dummy.getInstance(context);
        try (Session dummySession = dummy.createSession()) {
            // normally it should done on dummy (but ok, works on session anyway)
            Page ipnsEntry = IPFS.resolve(dummySession, ipfs.self(),
                    0, new TimeoutCancellable(120));
            assertNotNull(ipnsEntry);

            LogUtils.error(TAG, ipnsEntry.toString());
        }


    }
}
