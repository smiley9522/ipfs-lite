package threads.lite;


import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import threads.lite.cid.Cid;
import threads.lite.cid.IPV;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Network;
import threads.lite.core.PeerInfo;
import threads.lite.core.Reservation;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.lite.core.TimeoutCancellable;
import threads.lite.quic.Connection;
import threads.lite.quic.Parameters;
import threads.lite.quic.StreamRequester;


@RunWith(AndroidJUnit4.class)
public class IpfsRelaysTest {
    private static final String TAG = IpfsRelaysTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_connect_relays() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }


        if (ipfs.ipv().get() != IPV.IPv6) {
            LogUtils.warning(TAG, "nothing to test here NO IPv6 [Requirement for my pc]");
            return;
        }

        Server server = TestEnv.getServer();
        assertNotNull(server);

        String text = new String(TestEnv.getRandomBytes(10000));

        Cid cid;
        try (Session session = ipfs.createSession()) {
            cid = IPFS.storeText(session, text);
        }
        assertNotNull(cid);


        Set<Reservation> reservations = server.reservations();
        assertNotNull(reservations);
        assertFalse(reservations.isEmpty());


        Reservation reservation = reservations.iterator().next();
        assertNotNull(reservation);

        Multiaddr circuitMultiaddr = reservation.circuitAddress();
        assertNotNull(circuitMultiaddr);


        // now dial the multiaddr from a dummy
        Dummy dummy = Dummy.getInstance(context);

        try (Session dummySession = dummy.createSession()) {

            Connection connection = dummySession.dial(circuitMultiaddr, Parameters.getDefault());
            assertNotNull(connection);


            Multiaddr remoteAddress = StreamRequester.remoteMultiaddr(connection);
            assertFalse(remoteAddress.isCircuitAddress());

            LogUtils.error(TAG, "Remote connection " + remoteAddress);
            LogUtils.error(TAG, "Local dummy port " + connection.localAddress().getPort());

            // TEST 1 (Info about IPFS node)
            PeerInfo peerInfo = dummy.getHost().getPeerInfo(connection);
            assertNotNull(peerInfo);
            LogUtils.error(TAG, peerInfo.toString());


            // 1.2 (check agent and version)
            assertEquals(peerInfo.agent(), IPFS.AGENT);

            // 1.3 (check peerId) -> it is ipfs.self()
            assertEquals(peerInfo.peerId(), ipfs.self());

            // 1.4 (check protocols) -> it is ipfs server protocols
            List<String> protocols = Arrays.asList(peerInfo.protocols());
            assertTrue(protocols.contains(IPFS.LITE_PULL_PROTOCOL));
            assertTrue(protocols.contains(IPFS.LITE_PUSH_PROTOCOL));
            assertTrue(protocols.contains(IPFS.LITE_SWAP_PROTOCOL));
            assertTrue(protocols.contains(IPFS.MULTISTREAM_PROTOCOL));
            assertTrue(protocols.contains(IPFS.IDENTITY_PROTOCOL));
            assertTrue(protocols.contains(IPFS.IDENTITY_PUSH_PROTOCOL));
            assertTrue(protocols.contains(IPFS.BITSWAP_PROTOCOL));
            assertTrue(protocols.contains(IPFS.HOLE_PUNCH_PROTOCOL));
            assertTrue(protocols.contains(IPFS.RELAY_PROTOCOL_STOP));
            assertEquals(protocols.size(), 0);

            // 1.5 (check multiaddrs) Check that all returned multiaddrs are within
            // the servers dialable addresses
            Set<Multiaddr> dialable = server.dialableAddresses();
            List<Multiaddr> multiaddrs = Arrays.asList(peerInfo.multiaddrs());
            LogUtils.error(TAG, multiaddrs.toString());
            for (Multiaddr address : multiaddrs) {
                assertTrue(dialable.contains(address));
            }


            // TEST 2
            String cmp = IPFS.getText(dummySession, cid, new TimeoutCancellable(5));
            assertNotNull(cmp);
            assertEquals(cmp, text);


            // TODO [FUTURE low] when connection closed check if server and connections of dummy
            // is closed and cleaned


        }

        //  TODO [FUTURE low] create another dummy and check if reservation address is still working
        // assumption is that it is not working [but must be tested]


    }

}
