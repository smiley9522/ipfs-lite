package threads.lite;


import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;

import threads.lite.cid.Multiaddr;
import threads.lite.core.PeerInfo;
import threads.lite.core.Server;

@RunWith(AndroidJUnit4.class)
public class IpfsIdentifyServiceTest {
    private static final String TAG = IpfsIdentifyServiceTest.class.getSimpleName();


    @Test
    public void identify_test() throws Exception {

        Server server = TestEnv.getServer();
        TestCase.assertNotNull(server);

        PeerInfo info = IPFS.identity(server);
        assertNotNull(info);

        assertNotNull(info.agent());
        assertNotNull(info.peerId());

        Multiaddr[] list = info.multiaddrs();
        assertNotNull(list);
        for (Multiaddr addr : list) {
            LogUtils.info(TAG, addr.toString());
        }

        assertNull(info.observed());

    }
}
