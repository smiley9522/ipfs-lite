Version 3.2.5
-------------
- Support JDK version 17
- Support Android SDK 34
- Update dependencies (libraries)
- switch to immutable java records (jdk 16)
- use of pattern variables (jdk 16)
- support of enhanced switch statements (jdk 14)
- use CodedInputStream instead of ByteBuffer
- mplex deprecated (therefore noise not required anymore)
- simplify the sequence in ipns record (now it is System.currentTimeMillis() when creating)
- Identify (identify.proto) contains a SignedPeerRecord (envelope.proto, peer_record.proto)
- Support of own protocol "/lite/swap/1.0.0", used for faster data transfer instead of bitswap
- Strict quic-v1 support
- Removing of unnecessary wrapper classes (e.g. around protobuf)
- Identity ProtocolVersion will not be reported anymore
- QUIC: first steps to support Datagram [required for MASQUE]
- QUIC: Fix of server name extension [TLS]

Version 3.2.6
-------------
- QUIC: Optimize Memory usage
- Update dependencies (libraries)
- Remove Feature: Protocols can not be added dynamically [faster access to protocols]

Version 3.2.7
-------------
- UI : Submit less notifications to the user
- Feature : Define a sorting order on Multiaddr (might be important for dialing)
- Bugfix : Inline data for CID of type File, instead of creating always a Raw child node


Version 3.2.8
-------------
- API: Simplify MDNS and Swarm interface
- Feature: Introduce Request and Response message style starting with own protocols
- API: Update network has to be triggered from the outside (when network changed,
fixes a bug, that also a server should be triggered)
- API: Server publish in identity only public [dialable] addresses (before it also publish
circuit addresses when available)
- API: Own identity [IdentityHandler] of a client or server connection are now pre-stored [faster access]
- API: Identify [IdentityHandler] does not include the observed address anymore [faster generation and faster access]
- Feature: Initial version of upnp [not yet active]
- Feature: better ALPN support for QUIC [plan to use ALPN different from libp2p for own protocols]
- MDNS: Protocols is now part of the mdns message [not according to libp2p spec]
- MDNS: Support of registration service
- MDNS: Kubo mdns is not supported anymore (due to bad kubo performance)
- API: fixes + coding style issues
- Android: MinSdk Version is now 29 (Android Q) This enables the use of a faster DNS Resolver,
 which is non blocking [though not yet used, but will be important for virtual threads]
- API: DnsResolver based on Android DnsResolver (MinSdk 29)


Version 3.2.9
-------------
- API: Finish Lite ALPN (request and response style)
- API: Optimization memory and performance
- API: Slowly migrate API to be used for virtual threads [no/less completable future]
- API: feat(ipns): refactored IPNS package with lean records (https://github.com/ipfs/boxo/pull/339)

Version 3.3.0
-------------
- Update Libraries